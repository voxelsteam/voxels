#ifndef SCENE_H
#define SCENE_H

#include <QtOpenGL>
#include <QDebug>
#include "glwidget.h"
#include "voxelengine.h"
#include "renderengine.h"
#include "inputoutputservice.h"
#include "foraminifera.h"

class Camera;
class Scene
{

public:
    Scene(GLWidget *parent = 0);
    ~Scene();

    void setup_scene(const ForamParams& params);

    void render();

    Camera* get_camera() { return this->render_engine->get_camera(); }
    void intersect(bool negative);
    void reset();

    bool div_voxels();
    bool mul_voxels();
    void reset_voxels_multiplier() { voxels_multiplier = 1; }

    VoxelEngine *voxel_engine;
    RenderEngine *render_engine;
    Foraminifera *foraminifera = nullptr;
    GLWidget *parent;

    ForamParams load_parameters(QString name);
    void save_parameters(QString name);
    void save_amira(const QString& filename);
    void save_obj(const QString &filename);

    GLfloat voxels_multiplier;
    InputOutputService* get_objHandler();
private:
    InputOutputService* inputOutputService;
};

#endif // SCENE_H
