#version 120

uniform vec3 topColor;
uniform vec3 bottomColor;

attribute vec3 posAttr;

void main(){
    gl_Position = vec4(posAttr, 1.0);
}
