#version 120

attribute vec3 posAttr;
attribute vec3 normAttr;

varying vec3 f_fragPos;
varying vec3 f_normal;

struct Light
{
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform Light light;
uniform vec3 cameraPosition;
uniform mat4 transformation;

void main()
{
    gl_Position = transformation * vec4(posAttr, 1);
    f_fragPos = posAttr;
    f_fragPos.z = -f_fragPos.z;
    f_normal = normAttr;
}
