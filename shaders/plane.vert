#version 120

attribute vec3 posAttr;
uniform mat4 transformation;

void main() {
    gl_Position =  transformation * vec4(posAttr, 1.0);
}
