#version 120

varying vec3 f_fragPos;
varying vec3 f_normal;

struct Light {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform Light light;
uniform int shininess;
uniform vec3 cameraPosition;
uniform float gamma;
uniform float contrast;

void main() {

    vec3 material_color = vec3(0.73,0.73,0.73);

    //ambient
    vec3 ambient = light.ambient * material_color;

    //diffuse
    vec3 norm = normalize(f_normal);
    vec3 light_dir = normalize(-cameraPosition);
    float diff = max(dot(norm, light_dir), 0.0);
    vec3 diffuse = diff * (light.diffuse * material_color);

    //specular
    vec3 specular_dir = normalize(-cameraPosition - f_fragPos);
    vec3 reflect_dir = reflect(-specular_dir, norm);
    float spec = pow(max(dot(specular_dir, reflect_dir), 0.0), shininess);
    vec3 specular = light.specular * spec;

    //combine lighting
    vec3 result_light = ambient + diffuse + specular;
    gl_FragColor = vec4(result_light, 1.0f);

    //post processing
    gl_FragColor.rgb = ((gl_FragColor.rgb - 0.5f) * max(contrast, 0)) + 0.5f;
    gl_FragColor.rgb = pow(gl_FragColor.rgb, vec3(1.0/gamma));
}
