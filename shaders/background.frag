#version 120

uniform vec3 topColor;
uniform vec3 bottomColor;
uniform vec2 resolution;

void main()
{
    vec2 position = (gl_FragCoord.xy)/(resolution.y*.75);
    vec4 top = vec4(topColor, 1.0);
    vec4 bottom = vec4(bottomColor, 1.0);

    gl_FragColor = vec4(mix(bottom, top, position.y));
}
