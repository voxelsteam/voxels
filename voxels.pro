QT       += opengl core gui
QMAKE_CXXFLAGS += -std=c++11

greaterThan(QT_MAJOR_VERSION, 5.5): QT += widgets

TARGET = Forams
TEMPLATE = app

SOURCES += main.cpp\
    mainwindow.cpp \
    glwidget.cpp \
    voxelengine.cpp \
    scene.cpp \
    renderengine.cpp \
    config.cpp \
    camera.cpp \
    foraminifera.cpp \
    sparsematrix3d.cpp \
    progressbar.cpp \
    inputoutputservice.cpp \
    settingswindow.cpp

HEADERS  += mainwindow.h \
    glwidget.h \
    voxelengine.h \
    scene.h \
    renderengine.h \
    config.h \
    camera.h \
    foraminifera.h \
    sparsematrix3d.h \
    progressbar.h \
    settingswindow.h \
    inputoutputservice.h

FORMS    += mainwindow.ui \
    progressbar.ui \
    settingswindow.ui

DISTFILES += \
    .gitignore

RESOURCES += \
    shaders.qrc \
    icons.qrc \
    stylesheets.qrc
    win32:RC_ICONS += icon.ico
