#ifndef PROGRESSBAR_H
#define PROGRESSBAR_H

#include <QDialog>
#include <QDebug>

namespace Ui {
class ProgressBar;
}

class ProgressBar : public QDialog
{
    Q_OBJECT

public:
    explicit ProgressBar(QWidget *parent = 0);
    void setName(QString name);
    ~ProgressBar();

public slots:
    void progressChanged(int value);

private:
    Ui::ProgressBar *ui;
};

#endif // PROGRESSBAR_H
