#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <QDialog>
#include "scene.h"

namespace Ui {
class SettingsWindow;
}

class SettingsWindow : public QDialog
{
    Q_OBJECT
signals:
    void updateMarchingCubesState();

public:
    explicit SettingsWindow(GLWidget *openGL_widget, QWidget *parent = 0);
    ~SettingsWindow();

private slots:

    void on_buttonBox_accepted();

private:
    Ui::SettingsWindow *ui;
    GLWidget *openGL_widget;
};

#endif // SETTINGSWINDOW_H
