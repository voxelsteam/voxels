#include "progressbar.h"
#include "ui_progressbar.h"

ProgressBar::ProgressBar(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProgressBar)
{
    ui->setupUi(this);
    setWindowTitle("Export progress");
    setFixedSize(QSize(320, 120));
    setWindowFlags(Qt::WindowStaysOnTopHint);
}

ProgressBar::~ProgressBar()
{
    delete ui;
}

void ProgressBar::setName(QString name)
{
    QString tmp = ui->label->text();
    tmp.append(name);
    ui->label->setText(tmp);
}

void ProgressBar::progressChanged(int value)
{
    ui->progressBar->setValue(value);
}
