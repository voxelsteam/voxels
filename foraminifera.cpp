#include "foraminifera.h"

Foraminifera::Foraminifera(const ForamParams& params, GLfloat voxels_multiplier, VoxelEngine* voxel_engine):
    voxel_engine(voxel_engine), voxels_multiplier(voxels_multiplier)
{
    this->params = params;

    WORLD_SIZE = Config::instance()->world_size;
    filled_spaces = new SparseMatrix3D(128, WORLD_SIZE/128); // TODO: config

    generate_foraminifera();
}

Foraminifera::~Foraminifera()
{
    delete filled_spaces;
}

/**
 * Generates foraminifera if it wasn't generated earlier
 * @brief Foraminifera::get_foraminifera_voxels
 * @return
 */
void Foraminifera::generate_foraminifera()
{
    foraminifera_voxels.clear();
    foraminifera_voxels.resize(params.num_of_chambers);

    foraminifera_aperture_holes.clear();
    foraminifera_aperture_holes.resize(params.num_of_chambers);

    centers.clear();

    //start with defined shape, size and calculate aperture point
    QVector3D chamber_center(WORLD_SIZE/2, WORLD_SIZE/2, WORLD_SIZE/2); //put center chamber in the world center
    centers.append(chamber_center);
    QVector3D prev_chamber_center = chamber_center;
    QVector3D chamber_size = params.first_chamber_size * voxels_multiplier;
    prev_aperture = chamber_center;

    //calculate value of chamber thichness
    GLfloat current_chamber_thickness = qMax(qMax(chamber_size.x(), chamber_size.y()), chamber_size.z()) * params.chamber_thickness;

    if (current_chamber_thickness < 1){
        current_chamber_thickness = 1;
    }

    current_aperture = generate_chamber(0, chamber_center, chamber_size, current_chamber_thickness, LOCAL_MINIMALIZATION);

    //remember first center and first aperture
    QVector3D first_center = chamber_center;
    QVector3D first_aperture = current_aperture;

    GLfloat max_length = chamber_size.y();

    float deviation_angle = params.deviation_angle * M_PI;
    float rotation_angle = params.rotation_angle * M_PI;

    for(GLuint i = 1; i < params.num_of_chambers; i++)
    {
        //calculate new growth vector by deviating it from reference growth axis and rotating around it
        QVector3D growth_vector = deviate_and_rotate(deviation_angle, rotation_angle);

        max_length =  max_length * params.chamber_scaling_rates.y() ; //l i max

        //calculate new chamber center
        prev_chamber_center = chamber_center;
        chamber_center = current_aperture + (growth_vector * (params.growth_vector_scaling_rate * max_length ));
        centers.append(chamber_center);
        l_center += (chamber_center - prev_chamber_center).length();

        //calculate new chamber shape
        chamber_size = chamber_size * params.chamber_scaling_rates;

        current_chamber_thickness = qMax(qMax(chamber_size.x(), chamber_size.y()), chamber_size.z()) * params.chamber_thickness;

        if (current_chamber_thickness < 1){
            current_chamber_thickness = 1;
        }

        //generate new chamber
        prev_aperture = current_aperture;
        current_aperture = generate_chamber(i, chamber_center, chamber_size, current_chamber_thickness, CLOSEST_TO_PREVIOUS_APERTURE);
        l_aperture += (current_aperture - prev_aperture).length();
    }

    foraminifera_voxels.resize(params.num_of_chambers);
    foraminifera_aperture_holes.resize(params.num_of_chambers);
    l_center_prime = (first_center - chamber_center).length();
    l_aperture_prime = (first_aperture - current_aperture).length();
}

/**
 * Adds foraminifera to world data structure till step given as parameter
 * @brief Foraminifera::add_foraminifera_to_world
 * @param world
 * @param till_step
 */
void Foraminifera::add_foraminifera_to_world(GLuint till_step)
{
    if(till_step > params.num_of_chambers - 1 || !till_step) till_step = foraminifera_voxels.length();

    SparseMatrix3D *world = voxel_engine->world;

    for(GLuint i = 0; i < till_step; i++)
        for(int j = 0; j < foraminifera_voxels[i].length(); j++)
            world->put((int)foraminifera_voxels[i][j].x(), (int)foraminifera_voxels[i][j].y(), (int)foraminifera_voxels[i][j].z(), true);

    for(GLuint i = 0; i < till_step; i++)
        for(int j = 0; j < foraminifera_aperture_holes[i].length(); j++)
            world->put((int)foraminifera_aperture_holes[i][j].x(), (int)foraminifera_aperture_holes[i][j].y(), (int)foraminifera_aperture_holes[i][j].z(), false);

    visualized_step = till_step;
}

bool Foraminifera::visualize_next_step()
{
    if(visualized_step < params.num_of_chambers)
    {
        visualize_step(visualized_step + 1);
        return false;
    }

    return true;
}

bool Foraminifera::visualize_prev_step()
{
    if(visualized_step > 1)
    {
        visualize_step(visualized_step - 1);
        return false;
    }

    return true;
}

void Foraminifera::visualize_step(GLuint step)
{
    timer.start();

    voxel_engine->clear_buffers();
    voxel_engine->clear_world();
    add_foraminifera_to_world(step);
    voxel_engine->construct_world();

    qDebug("[FORAMINIFERA] Visualized %d step. [%d ms]", step, timer.elapsed());
}

/**
 * Wrapper method for generating chamber, checks if arguments are correct, draws chamber and aperture hole
 * @brief Foraminifera::generate_chamber
 * @param center
 * @param radii
 * @param chamber_thickness
 * @return
 */
QVector3D Foraminifera::generate_chamber(GLuint chamber_num, const QVector3D center, const QVector3D radii, const GLuint chamber_thickness, int aperture_finding_method)
{
    //perform argument checks, because sometimes bizarre things can happen
    if(radii.x() > WORLD_SIZE/2 || radii.y() > WORLD_SIZE/2 || radii.z() > WORLD_SIZE/2){
        qDebug() << "Chamber radius is too big! Aborting";
        return QVector3D(0,0,0);
    }

    if(center.x() >= WORLD_SIZE || center.x() < 0 || center.y() >= WORLD_SIZE || center.y() < 0 || center.z() >= WORLD_SIZE || center.z() < 0)
    {
        qDebug() << "Chamber center is out of bounds! Aborting";
        return QVector3D(0,0,0);
    }

    if(chamber_thickness > radii.x() || chamber_thickness > radii.y() || chamber_thickness > radii.z())
    {
        qDebug() << "Chamber thickness is too big! Aborting";
        return QVector3D(0,0,0);
    }

    float minimal_point = INFINITY;
    QVector3D aperture(0,0,0);
    int x0 = (int)center.x();
    int y0 = (int)center.y();
    int z0 = (int)center.z();

    int radii_x = (int) radii.x();
    int radii_y = (int) radii.y();
    int radii_z = (int) radii.z();

    float one_over_radii_x_squared = 1.0f/(radii.x()*radii.x());
    float one_over_radii_y_squared = 1.0f/(radii.y()*radii.y());
    float one_over_radii_z_squared = 1.0f/(radii.z()*radii.z());

    float one_over_radii_inner_x_squared = 1.0f/((radii.x()-chamber_thickness)*(radii.x()-chamber_thickness));
    float one_over_radii_inner_y_squared = 1.0f/((radii.y()-chamber_thickness)*(radii.y()-chamber_thickness));
    float one_over_radii_inner_z_squared = 1.0f/((radii.z()-chamber_thickness)*(radii.z()-chamber_thickness));

    float EXPAND = 1.1; //keep as low as possible but above 1.0, anything below 1.1 is unsafe
    for(int i = x0 - (int)radii_x*EXPAND; i < x0 + (int)radii_x*EXPAND; i++)
    {
        for(int j = y0 - (int)radii_y*EXPAND; j < y0 + (int)radii_y*EXPAND; j++)
        {
            for(int k = z0 - (int)radii_z*EXPAND; k < z0 + (int)radii_z*EXPAND; k++)
            {
                if(i >= 0 && i < WORLD_SIZE && j >= 0 && j < WORLD_SIZE && k >= 0 && k < WORLD_SIZE)
                {
                    if (filled_spaces->get(i, j, k) == false)
                    {
                        //we will use ellipse equation to determine if point (i,j,k) lies inside ellipse or not
                        GLfloat ellipse_equation = (i - x0)*(i - x0)*one_over_radii_x_squared +
                                (j - y0)*(j - y0)*one_over_radii_y_squared +
                                (k - z0)*(k - z0)*one_over_radii_z_squared;
                        //the same but for inner ellipse
                        GLfloat inner_ellipse_equation = (i - x0)*(i - x0)*one_over_radii_inner_x_squared +
                                (j - y0)*(j - y0)*one_over_radii_inner_y_squared +
                                (k - z0)*(k - z0)*one_over_radii_inner_z_squared;

                        //if voxel belongs to chamber wall
                        if((chamber_thickness != 0 && ellipse_equation <= 1 && inner_ellipse_equation >= 1) || (chamber_thickness == 0 && ellipse_equation <= 1))
                        {
                            //put voxel to data structure that represents walls
                            foraminifera_voxels[chamber_num].push_back(QVector3D(i, j, k));

                            //find aperture
                            if(aperture_finding_method == LOCAL_MINIMALIZATION && minimal_point > ellipse_equation)
                            {
                                minimal_point = ellipse_equation;
                                aperture = QVector3D(i, j, k);
                            }
                            else if (aperture_finding_method == CLOSEST_TO_PREVIOUS_APERTURE)
                            {
                                //find distance between aperture of previous chamber and point (i,j,k)
                                float distance_squared = (prev_aperture.x()-i)*(prev_aperture.x()-i) + (prev_aperture.y()-j)*(prev_aperture.y()-j) + (prev_aperture.z()-k) * (prev_aperture.z()-k);
                                if(minimal_point > distance_squared)
                                {
                                    //if it is candidate of being minimum distance point, then check if new aperture is outside other chambers
                                    minimal_point = distance_squared;
                                    aperture = QVector3D(i, j, k);

                                }
                            }

                        }
                    }
                }
            }
        }
   }

    //draw aperture hole, this is by far the most efficient way to do that without having to calculate inverse trigonometric functions
    QVector3D aperture_center = aperture - center;
    for(int i = x0 - (int)radii_x*EXPAND; i < x0 + (int)radii_x*EXPAND; i++)
    {
        for(int j = y0 - (int)radii_y*EXPAND; j < y0 + (int)radii_y*EXPAND; j++)
        {
            for(int k = z0 - (int)radii_z*EXPAND; k < z0 + (int)radii_z*EXPAND; k++)
            {
                if(i >= 0 && i < WORLD_SIZE && j >= 0 && j < WORLD_SIZE && k >= 0 && k < WORLD_SIZE)
                {
                    GLfloat ellipse_equation = (i - x0)*(i - x0)*one_over_radii_x_squared +
                            (j - y0)*(j - y0)*one_over_radii_y_squared +
                            (k - z0)*(k - z0)*one_over_radii_z_squared;

                    //we should remember that point (i,j,k) is inside chamber
                    if(ellipse_equation < 1)
                    {
                        filled_spaces->put(i, j, k, true);
                        total_volume++;
                    }

                    if(ellipse_equation <= 1) //keep ray inside
                    {
                        QVector3D tmp(i,j,k);
                        QVector3D tmp_center = tmp - center;
                        float cos_angle = QVector3D::dotProduct(tmp_center, aperture_center)/(tmp_center.length()*aperture_center.length());
                        if(cos_angle > 0.98) foraminifera_aperture_holes[chamber_num].push_back(tmp);
                    }
                }
            }
        }
    }
    float planck_constant = 0.5f;

    //add random energy fluctuations to eliminate quantum field properties
    aperture.setX(aperture.x() + (((double) rand() / (RAND_MAX)) - 0.5f)*planck_constant);
    aperture.setY(aperture.y() + (((double) rand() / (RAND_MAX)) - 0.5f)*planck_constant);
    aperture.setZ(aperture.z() + (((double) rand() / (RAND_MAX)) - 0.5f)*planck_constant);
    return aperture;
}

/**
 * First deviates vector from reference axis by deviation_angle then rotates
 * deviated vector around reference axis by rotation_angle
 * @brief Foraminifera::deviate_and_rotate
 * @param reference_axis
 * @param deviation_angle
 * @param rotation_angle
 * @return
 */
QVector3D Foraminifera::deviate_and_rotate(float deviation_angle, float rotation_angle)
{
    //calculate direction
    QVector3D reference_axis_direction = current_aperture - prev_aperture;

    //deviate point from reference axis using normal vector
    QVector3D deviated_vector = rotate_around_arbitrary_line(reference_axis_direction, prev_aperture, current_aperture, deviation_angle);

    //now rotate it around reference axis
    QVector3D rotated_vector = rotate_around_arbitrary_line(deviated_vector, prev_aperture, current_aperture, rotation_angle);

    // adjust new vector, then return it
    rotated_vector.normalize();

    return rotated_vector;
}

/**
 * Rotates vector vec in respect to arbitrary line defined as line running through points axis_point_one and axis_point_two by angle given in radians
 * @brief Foraminifera::rotate_around_arbitrary_line
 * @param vec
 * @param axis_point_one
 * @param axis_point_two
 * @param angle
 * @return
 */
QVector3D Foraminifera::rotate_around_arbitrary_line(QVector3D vec, QVector3D axis_point_one, QVector3D axis_point_two, float angle)
{
    QVector3D dir = axis_point_two - axis_point_one;

    float a = axis_point_one.x();
    float b = axis_point_one.y();
    float c = axis_point_one.z();

    float u = dir.x();
    float v = dir.y();
    float w = dir.z();

    float x = vec.x();
    float y = vec.y();
    float z = vec.z();

    float l = u*u + v*v + w*w;
    float l_squared = sqrt(l);
    float one_over_l = 1.0f/l;
    float sin_angle = sin(angle);
    float cos_angle = cos(angle);

    //we will use formula below in order to get new rotated vector
    float rotated_x = ((a*(v*v+w*w)-u*(b*v+c*w-u*x-v*y-w*z))*(1-cos_angle)+l*x*cos_angle+l_squared*(-c*v+b*w-w*y+v*z)*sin_angle)*one_over_l;
    float rotated_y = ((b*(u*u+w*w)-v*(a*u+c*w-u*x-v*y-w*z))*(1-cos_angle)+l*y*cos_angle+l_squared*( c*u-a*w+w*x-u*z)*sin_angle)*one_over_l;
    float rotated_z = ((c*(u*u+v*v)-w*(a*u+b*v-u*x-v*y-w*z))*(1-cos_angle)+l*z*cos_angle+l_squared*(-b*u+a*v-v*x+u*y)*sin_angle)*one_over_l;

    return QVector3D(rotated_x, rotated_y, rotated_z);
}

/**
 * Returns N_center parameter
 * @brief Foraminifera::get_n_center
 * @return
 */
float Foraminifera::get_n_center()
{
    return l_center != 0 && l_center_prime != 0 ? l_center_prime/l_center : 0.0f;
}

/**
 * Retursn N_aperture parameter
 * @brief Foraminifera::get_n_aperture
 * @return
 */
float Foraminifera::get_n_aperture()
{
    return l_aperture_prime != 0 && l_aperture != 0 ? l_aperture_prime/l_aperture : 0.0f;
}

/**
 * Returns first point of minimal boundary box
 * @brief Foraminifera::get_min_rect_point_A
 * @return
 */
QVector3D Foraminifera::get_min_rect_point_A()
{
    if(foraminifera_voxels.empty()) return QVector3D(0,0,0);
    //min, min, min
    float min_x = INFINITY;
    float min_y = INFINITY;
    float min_z = INFINITY;
    for(int i = 0; i < foraminifera_voxels.length(); i++)
    {
        for(int j = 0; j < foraminifera_voxels[i].length(); j++)
        {
            if (foraminifera_voxels[i][j].x() < min_x) min_x = foraminifera_voxels[i][j].x();
            if (foraminifera_voxels[i][j].y() < min_y) min_y = foraminifera_voxels[i][j].y();
            if (foraminifera_voxels[i][j].z() < min_z) min_z = foraminifera_voxels[i][j].z();
        }
    }
    return QVector3D(min_x, min_y, min_z);
}

/**
 * Returns second point of minimal boundary box
 * @brief Foraminifera::get_min_rect_point_B
 * @return
 */
QVector3D Foraminifera::get_min_rect_point_B()
{
    if(foraminifera_voxels.empty()) return QVector3D(0,0,0);
    //max, max, max
    float max_x = -INFINITY;
    float max_y = -INFINITY;
    float max_z = -INFINITY;
    for(int i = 0; i < foraminifera_voxels.length(); i++)
    {
        for(int j = 0; j < foraminifera_voxels[i].length(); j++)
        {
            if (foraminifera_voxels[i][j].x() > max_x) max_x = foraminifera_voxels[i][j].x();
            if (foraminifera_voxels[i][j].y() > max_y) max_y = foraminifera_voxels[i][j].y();
            if (foraminifera_voxels[i][j].z() > max_z) max_z = foraminifera_voxels[i][j].z();
        }
    }
    return QVector3D(max_x, max_y, max_z);
}

/**
 * Retrieves volume of foraminifera with empty spaces inside
 * @brief Foraminifera::get_total_volume
 * @return
 */
int Foraminifera::get_material_volume()
{
    int material_volume = 0;
    for(GLuint i = 0; i < params.num_of_chambers; i++)
        material_volume += foraminifera_voxels[i].length();
    return material_volume;
}

int Foraminifera::count_free_walls(int x, int y, int z)
{
    int sum = 0;
    if (!filled_spaces->get(x-1, y, z)) sum++;
    if (!filled_spaces->get(x+1, y, z)) sum++;
    if (!filled_spaces->get(x, y-1, z)) sum++;
    if (!filled_spaces->get(x, y+1, z)) sum++;
    if (!filled_spaces->get(x, y, z-1)) sum++;
    if (!filled_spaces->get(x, y, z+1)) sum++;
    return sum;
}

/**
 * Retrieves surface area of foraminifera
 * @brief Foraminifera::get_surface_area
 * @return
 */
int Foraminifera::get_surface_area()
{
    int fsum = 0;
    for(int i = 0; i < (int)foraminifera_voxels.length(); i++)
        for(int j = 0; j < foraminifera_voxels[i].length(); j++)
            fsum += count_free_walls(foraminifera_voxels[i][j].x(), foraminifera_voxels[i][j].y(), foraminifera_voxels[i][j].z());
    return fsum;
}

float distance(QVector3D point, QVector3D center, QVector3D chamber_size)
{
    float delta_x = (point.x() - center.x())*(point.x() - center.x()) / (chamber_size.x()*chamber_size.x());
    float delta_y = (point.y() - center.y())*(point.y() - center.y()) / (chamber_size.y()*chamber_size.y());
    float delta_z = (point.z() - center.z())*(point.z() - center.z()) / (chamber_size.z()*chamber_size.z());
    return delta_x + delta_y + delta_z;
}

QVector<QVector<float> > Foraminifera::stepped_voxels()
{
    SparseMatrix3D* tmp_world = voxel_engine->world;
    QVector<QVector<float>> world;
    QVector3D curr_chamber_size = params.first_chamber_size;
    for (uint i = 0; i < params.num_of_chambers; i++)
    {
        for(int j = 0; j < foraminifera_voxels[i].length(); j++)
        {
            int x = (int)foraminifera_voxels[i][j].x();
            int y = (int)foraminifera_voxels[i][j].y();
            int z = (int)foraminifera_voxels[i][j].z();
            if (tmp_world->get(x,y,z))
            {
                QVector<float> vec;
                vec.append(x);
                vec.append(y);
                vec.append(z);
                vec.append(i);
                vec.append(distance(QVector3D(x, y, z), centers[i], curr_chamber_size));
                vec.append(voxel_engine->is_in_the_middle(x, y, z));
                world.append(vec);
            }
        }
        curr_chamber_size *= params.chamber_scaling_rates;
    }
    qDebug() << "foram ended";
    return world;
}

