#include "glwidget.h"
#include "scene.h"
#include "mainwindow.h"
#include "camera.h"

GLWidget::GLWidget(QWidget *parent):
    QOpenGLWidget(parent), x_mouse_sens(Config::instance()->x_mouse_sens),
    y_mouse_sens(Config::instance()->y_mouse_sens), wheel_sens(Config::instance()->wheel_sens),
    date_format("dd.MM.yyyy hh.mm.ss.zzz"), screen_dir(Config::instance()->screen_dir),
    screen_core(Config::instance()->screen_core), aspect_ratio(1), num_of_samples(4)

{
    QSurfaceFormat format;
    format.setSamples(num_of_samples);
    this->setFormat(format);

    setMouseTracking(true);
}

GLWidget::~GLWidget()
{
    delete scene;
    scene = nullptr;
}

void GLWidget::initializeGL()
{
    initializeOpenGLFunctions();

    scene = new Scene(this);
    camera = scene->get_camera();
}

void GLWidget::paintGL()
{
    double current_ratio = width()/(double)height();
    glViewport(0, 0, width(), height());

    if(current_ratio != aspect_ratio) scene->render_engine->set_resolution(width(), height());

     scene->render();
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    last_mouse_pos = event->pos();
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    Qt::MouseButtons button_state = event->buttons();
    GLint dx_mouse = event->x() - last_mouse_pos.x();
    GLint dy_mouse = event->y() - last_mouse_pos.y();

    if((event->modifiers() & Qt::KeyboardModifier::ShiftModifier) == Qt::KeyboardModifier::ShiftModifier)
    {
        scene->render_engine->set_plane(event->x(), event->y());
    }

    if ((button_state & Qt::RightButton) == Qt::RightButton)
    {
        // calculate rotation angles around each axis
        GLfloat y_rot_angle = normalize_angle(dx_mouse * x_mouse_sens);
        GLfloat x_rot_angle = normalize_angle(dy_mouse * y_mouse_sens);

        camera->rotate(QVector3D(x_rot_angle, y_rot_angle, 0));
    }
    else if ((button_state & Qt::MiddleButton) == Qt::MiddleButton)
    {
        camera->look_around(dx_mouse * x_mouse_sens * 0.05, -dy_mouse * y_mouse_sens * 0.05);
    }
    else if ((button_state & Qt::LeftButton) == Qt::LeftButton)
    {
        camera->move(QVector3D(dx_mouse * x_mouse_sens * 0.05, -dy_mouse * y_mouse_sens * 0.05, 0));
    }

    update();
    last_mouse_pos = event->pos();
}

void GLWidget::wheelEvent(QWheelEvent *event)
{
    GLfloat factor = event->delta() * wheel_sens;
    camera->zoom(-factor);

    update();
}

GLfloat GLWidget::normalize_angle(GLfloat angle)
{
    while (angle < 0)
        angle += 360;
    while (angle > 360)
        angle -= 360;

    return angle;
}

void GLWidget::screenshot()
{
    if (!QDir(screen_dir).exists()) QDir().mkdir(screen_dir);
    QString fileName = screen_dir + "/" + screen_core + " " + QDateTime::currentDateTime().toString(date_format) + ".png";

    QWidget* parent = qobject_cast<QWidget*>(this->parent());
    GLint x = parent->x();
    GLint y = parent->y();
    GLint w = parent->width();
    GLint h = parent->height();

    uchar* pixels = new uchar[3 * w * h];
    glReadPixels(x, y, w - 1, h - 1, GL_RGB, GL_UNSIGNED_BYTE, pixels);

    QImage screenshot(pixels, w - 1, h - 1, QImage::Format_RGB888);
    screenshot = screenshot.mirrored();
    screenshot.save(fileName);

    delete [] pixels;
    qDebug() << "Screenshot saved as " << fileName;
}
