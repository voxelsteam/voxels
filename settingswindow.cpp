#include "settingswindow.h"
#include "ui_settingswindow.h"

SettingsWindow::SettingsWindow(GLWidget *openGL_widget, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsWindow), openGL_widget(openGL_widget)
{
    ui->setupUi(this);

    ui->ambientSlider->setValue(openGL_widget->get_scene()->render_engine->light->ambient.x()*100);
    ui->diffuseSlider->setValue(openGL_widget->get_scene()->render_engine->light->diffuse.x()*100);
    ui->specularSlider->setValue(openGL_widget->get_scene()->render_engine->light->specular.x()*100);

    ui->marchingCubesBox->setChecked(openGL_widget->get_scene()->voxel_engine->marching_cubes_enabled);
}

SettingsWindow::~SettingsWindow()
{
    delete ui;
}

void SettingsWindow::on_buttonBox_accepted()
{
    openGL_widget->get_scene()->render_engine->light->ambient = QVector3D(1.0f, 1.0f, 1.0f) * ui->ambientSlider->value() * 0.01;
    openGL_widget->get_scene()->render_engine->light->diffuse = QVector3D(1.0f, 1.0f, 1.0f) * ui->diffuseSlider->value() * 0.01;
    openGL_widget->get_scene()->render_engine->light->specular = QVector3D(1.0f, 1.0f, 1.0f) * ui->specularSlider->value() * 0.01;

    if(openGL_widget->get_scene()->voxel_engine->marching_cubes_enabled != ui->marchingCubesBox->isChecked()){

        if (ui->marchingCubesBox->isChecked()){
            openGL_widget->get_scene()->voxel_engine->marching_cubes_enabled=true;
        }
        else{
            openGL_widget->get_scene()->voxel_engine->marching_cubes_enabled=false;
        }
        emit(updateMarchingCubesState());

    }
}

