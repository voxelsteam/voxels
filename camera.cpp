#include "camera.h"

Camera::Camera(): fov(Config::instance()->fov), z_near(Config::instance()->z_near),
    z_far(Config::instance()->z_far), aspect_ratio(1.33)
{
    look_at_left_multiplier = new QMatrix4x4;
    look_at_left_multiplier->setRow(3, QVector4D(0, 0, 0, 1));

    look_at_right_multiplier = new QMatrix4x4;
    look_at_right_multiplier->setToIdentity();

    look_at = new QMatrix4x4;

    rotation_matrix = new QMatrix4x4;
    rotation_matrix->setToIdentity();

    projection_matrix = new QMatrix4x4;
    projection_matrix->setToIdentity();

    calculate_projection_matrix();

    zoom(350); // temporary
}

Camera::~Camera()
{
    delete look_at_left_multiplier;
    look_at_left_multiplier = nullptr;

    delete look_at_right_multiplier;
    look_at_right_multiplier = nullptr;

    delete look_at;
    look_at = nullptr;

    delete rotation_matrix;
    rotation_matrix = nullptr;

    delete projection_matrix;
    projection_matrix = nullptr;
}

QMatrix4x4 Camera::get_matrix()
{
    QVector3D direction = camera_position - target_position;
    direction.normalize();

    camera_right = QVector3D::crossProduct(world_up, direction);
    camera_up = QVector3D::crossProduct(direction, camera_right);

    look_at_left_multiplier->setRow(0, QVector4D(camera_right, 0));
    look_at_left_multiplier->setRow(1, QVector4D(camera_up, 0));
    look_at_left_multiplier->setRow(2, QVector4D(direction, 0));

    look_at_right_multiplier->setColumn(3, QVector4D(camera_position, 1));

    // calculate camera projection
    *look_at = *look_at_left_multiplier * *look_at_right_multiplier;

    return *projection_matrix * *look_at;
}

void Camera::zoom(float factor)
{
    distance += distance * factor;
    camera_default_position.setZ(distance);

    camera_position = (camera_default_position + translation) * *rotation_matrix;
    look_around(0, 0);
}

void Camera::move(const QVector3D &vector)
{
    QVector3D translation_rotated = vector * *rotation_matrix;
    translation += vector;

    camera_position += translation_rotated;
    target_position +=translation_rotated;
}

void Camera::look_around(GLfloat yaw, GLfloat pitch)
{
    if(pitch > 89.0f) pitch =  89.0f;
    if(pitch < -89.0f) pitch = -89.0f;
    if(yaw > 89.0f) yaw = 89.0f;
    if(yaw < -89.0f) yaw = -89.0f;

    this->yaw += qDegreesToRadians(yaw);
    this->pitch += qDegreesToRadians(pitch);

    GLfloat xz_hypotenuse = distance / qCos(this->yaw);
    GLfloat yz_hypotenuse = distance / qCos(this->pitch);

    unrotated_target_position.setX(qSin(this->yaw) * xz_hypotenuse);
    unrotated_target_position.setY(qSin(this->pitch) * yz_hypotenuse);

    target_position = (unrotated_target_position + translation)* *rotation_matrix;
}

void Camera::rotate(const QVector3D &angles)
{
    // calculate rotation matrix
    QMatrix4x4 x_rot_matrix, y_rot_matrix, z_rot_matrix;
    x_rot_matrix.setToIdentity();
    x_rot_matrix.rotate(angles.x(), 1, 0, 0);
    y_rot_matrix.setToIdentity();
    y_rot_matrix.rotate(angles.y(), 0, 1, 0);
    z_rot_matrix.setToIdentity();
    z_rot_matrix.rotate(angles.z(), 0, 0, 1);
    *rotation_matrix = x_rot_matrix * y_rot_matrix * z_rot_matrix * *rotation_matrix;

    // rotate camera
    camera_position = (camera_default_position + translation) * *rotation_matrix;
    world_up = world_default_up* *rotation_matrix;

    target_position = (unrotated_target_position + translation)* *rotation_matrix;
}

void Camera::reset()
{
    rotation_matrix->setToIdentity();
    world_up = QVector3D(0, 1, 0);
    target_position = QVector3D(0, 0, 0);

    camera_position = QVector3D(0, 0, 0);
    camera_default_position = QVector3D(0, 0, 1);
    distance = 1.0f;

    yaw = 0;
    pitch = 0;
    unrotated_target_position = QVector3D(0, 0, 0);
    translation = QVector3D(0, 0, 0);
    zoom(Config::instance()->start_camera_distance);
}

void Camera::calculate_projection_matrix()
{
    float tangent = qTan(fov/2);
    float z_range = z_near - z_far;

    this->projection_matrix->setRow(0, QVector4D(1.0f/(tangent * aspect_ratio), 0.0f,0.0f, 0.0f));
    this->projection_matrix->setRow(1, QVector4D(0.0f, 1.0f/tangent, 0.0f, 0.0f));
    this->projection_matrix->setRow(2, QVector4D(0.0f, 0.0f, (-z_near - z_far)/z_range, 2*z_far*z_near/z_range));
    this->projection_matrix->setRow(3, QVector4D(0.0f, 0.0f, 1.0f, 0.0f));
}
