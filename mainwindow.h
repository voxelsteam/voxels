#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "config.h"
#include "scene.h"
#include "settingswindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void export_in_progress();
    void export_ended();

private slots:

    void on_SimuateButton_clicked();

    void on_PrevButton_clicked();

    void on_NextButton_clicked();

    void on_ImportButton_clicked();

    void on_ExportButton_clicked();

    void on_ResetButton_clicked();

    void on_PrintScreenButton_clicked();

    void on_intersectButton_clicked();

    void on_intersectNegativeButton_clicked();

    void on_intersectResetButton_clicked();

    void on_planeToggleButton_clicked();

    void on_voxelsDivButton_clicked();

    void on_voxelsMulButton_clicked();

    void on_aboutButton_clicked();

    void on_pushButton_clicked();

    void on_settingsButton_clicked();

private:
    bool connected;
    Ui::MainWindow *ui;
    Scene* scene;
    SettingsWindow* settingsWindow;

    void closeEvent(QCloseEvent* event);
    void showEvent(QShowEvent*);

    void setTitleLabel(const QString& labelName, QDockWidget* dockWidget);
    void configureDockWidget(QDockWidget* dockWidget);
    void createDockWidget(QWidget *widget, const QString& labelName, bool leftArea);
    void fillOutputTable();
    void simulate();
};

#endif // MAINWINDOW_H
