#ifndef SPARSEMATRIX3D_H
#define SPARSEMATRIX3D_H

class SparseMatrix3D
{

public:
    SparseMatrix3D(int chunks_number, int chunk_size);
    ~SparseMatrix3D();

    bool get(int x, int y, int z);
    bool get(int x, int y, int z, int ch_x, int ch_y, int ch_z);
    void put(int x, int y, int z, bool value = true);
    bool****** chunks;
    int chunk_size;
    int chunks_number;

private:
};

#endif // SPARSEMATRIX3D_H
