#include "sparsematrix3d.h"

SparseMatrix3D::SparseMatrix3D(int chunks_number, int chunk_size):
    chunks_number(chunks_number), chunk_size(chunk_size)
{
    chunks = new bool*****[chunks_number];
    for(int i = 0; i < chunks_number; i++)
    {
        chunks[i] = new bool****[chunks_number];
        for(int j = 0; j < chunks_number; j++)
        {
            chunks[i][j] = new bool***[chunks_number];
            for(int k = 0; k < chunks_number; k++)
                chunks[i][j][k] = nullptr;
        }
    }
}

SparseMatrix3D::~SparseMatrix3D()
{
    for(int i=0;i<chunks_number;i++)
    {
      for(int j=0;j<chunks_number;j++)
      {
          for(int k = 0; k < chunks_number; k++)
          {
              bool*** chunk = chunks[i][j][k];
              if(chunk != nullptr)
              {
                for(int ch_i = 0; ch_i < chunk_size; ch_i++)
                {
                    for(int ch_j = 0; ch_j < chunk_size; ch_j++)
                    {
                        delete [] chunk[ch_i][ch_j];
                    }
                    delete [] chunk[ch_i];
                }
                delete [] chunk;
                chunk = nullptr;
              }
          }
          delete [] chunks[i][j];
      }
      delete [] chunks[i];
    }
    delete [] chunks;
}

bool SparseMatrix3D::get(int x, int y, int z)
{
    short chunks_x = x/chunk_size, chunks_y = y/chunk_size, chunks_z = z/chunk_size;
    bool*** chunk = chunks[chunks_x][chunks_y][chunks_z];

    if(chunk == nullptr) return false;

    int chunk_x = x - chunks_x * chunk_size;
    int chunk_y = y - chunks_y * chunk_size;
    int chunk_z = z - chunks_z * chunk_size;
    if(chunk_x < 0 || chunk_x >= chunk_size || chunk_y < 0 || chunk_y >= chunk_size || chunk_z < 0 || chunk_z >= chunk_size) return false;

    return chunk[chunk_x][chunk_y][chunk_z];
}

bool SparseMatrix3D::get(int x, int y, int z, int ch_x, int ch_y, int ch_z)
{
    if(x < 0 || x >= chunks_number || y < 0 || y >= chunks_number || z < 0 || z >= chunks_number) return false;

    bool*** chunk = chunks[x][y][z];
    if(chunk == nullptr) return false;

    return chunk[ch_x][ch_y][ch_z];
}

void SparseMatrix3D::put(int x, int y, int z, bool value)
{
    short chunks_x = x/chunk_size, chunks_y = y/chunk_size, chunks_z = z/chunk_size;
    if(chunks_x >= chunks_number || chunks_y >= chunks_number || chunks_z >= chunks_number) return; // silently return...
    bool*** chunk = chunks[chunks_x][chunks_y][chunks_z];

    if(chunk == nullptr)
    {
        chunk = new bool**[chunk_size];
        for(int i =0; i<chunk_size; i++)
        {
            chunk[i] = new bool*[chunk_size];
            for(int j =0; j<chunk_size; j++)
            {
                chunk[i][j] = new bool[chunk_size];
                for(int k = 0; k<chunk_size; k++)
                {
                    chunk[i][j][k] = 0;
                }
            }
        }
    }

    chunk[x - chunks_x * chunk_size][y - chunks_y * chunk_size][z - chunks_z * chunk_size] = value;
    chunks[chunks_x][chunks_y][chunks_z] = chunk;
}
