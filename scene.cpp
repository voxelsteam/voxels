#include "scene.h"

/**
 * @brief Scene::Scene used to store all objects required for backend logic and to separate glwidget functionality
 * @param parent
 */
Scene::Scene(GLWidget *parent): voxels_multiplier(1)
{
    this->parent = parent;
    this->render_engine = new RenderEngine(parent);
    this->voxel_engine = render_engine->get_voxel_engine();
    this->inputOutputService = new InputOutputService();
}

Scene::~Scene()
{
    delete render_engine;
    render_engine = nullptr;

    delete inputOutputService;
    inputOutputService = nullptr;

    delete foraminifera;
    foraminifera = nullptr;
}

/**
 * @brief Scene::setup_scene function used to create voxels for the scene
 */
void Scene::setup_scene(const ForamParams& params)
{
    this->voxel_engine->clear_buffers();

    delete this->foraminifera;
    this->foraminifera = new Foraminifera(params, voxels_multiplier, voxel_engine);
    this->voxel_engine->initialize_world(Config::instance()->world_size);
    this->foraminifera->add_foraminifera_to_world();
    this->voxel_engine->construct_world();
    this->render_engine->update();
}

/**
 * @brief Scene::render Used to delegate rendering logic to engine
 */
void Scene::render()
{
    render_engine->render();
}

void Scene::intersect(bool negative)
{
    parent->makeCurrent();

    voxel_engine->clear_world();
    foraminifera->add_foraminifera_to_world(foraminifera->visualized_step);
    voxel_engine->intersect(negative);
    voxel_engine->clear_buffers();
    voxel_engine->construct_world();
    render_engine->update();

    parent->doneCurrent();
}

void Scene::reset()
{
    parent->makeCurrent();

    foraminifera->add_foraminifera_to_world(foraminifera->visualized_step);
    voxel_engine->clear_buffers();
    voxel_engine->construct_world();
    render_engine->update();

    parent->doneCurrent();
}

ForamParams Scene::load_parameters(QString name)
{
    return inputOutputService->load_parameters(name);
}

void Scene::save_parameters(QString name)
{
    if(foraminifera != nullptr) inputOutputService->save_parameters(name, foraminifera->get_params());
}

void Scene::save_amira(const QString& filename)
{
    inputOutputService->save_amira(filename, foraminifera->stepped_voxels());
}

void Scene::save_obj(const QString &filename)
{
    inputOutputService->save_obj(filename, voxel_engine->vertices_list, voxel_engine->indices_list);
}

bool Scene::div_voxels()
{
   if(voxels_multiplier / 2 < 1/(float)4) return false;
   voxels_multiplier /= 2;

   return true;
}

bool Scene::mul_voxels()
{
  if(voxels_multiplier * 2 > 2) return false;
  voxels_multiplier *= 2;

  return true;
}

InputOutputService* Scene::get_objHandler()
{
    return inputOutputService;
}
