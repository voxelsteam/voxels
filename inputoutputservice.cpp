#include "inputoutputservice.h"

ProgressBar* InputOutputService::obar;
ProgressBar* InputOutputService::abar;

ObjExporter* ObjExporter::inst = NULL;
int ObjExporter::count = 0;

void ObjExporter::clear_instance()
{
    inst = NULL;
}

ObjExporter* ObjExporter::instance()
{
    if (!inst) {
        inst = new ObjExporter;
        InputOutputService::obar = new ProgressBar();
    }
    return inst;
}

bool ObjExporter::export_file()
{
    qDebug() << "starting thread " << this;
    QFile file(filename);
    if ( file.open(QIODevice::ReadWrite) )
    {
        int sumlen = ver.length();
        foreach (QVector<GLushort>* vec , *fac)
        {
            sumlen += vec->length();
        }
        sumlen /= 3;
        QTextStream stream( &file );
        stream << "# OBJ file format with ext .obj" << endl;
        stream << "# vertex count = " << (ver.size() / 3) << endl;
        stream << "# face count = " << (fac->size() / 3) << endl;
        int last_i = 0;
        for (int i = 0; i < ver.size() / 3; i++)
        {
            stream << "v";
            for (int j = 0; j < 3; j++)
            {
                stream << " " << ver.at(i * 3 + j);
            }
            stream << endl;
            int var = int((((i + 1) * 100) / sumlen));
            if (var != last_i)
            {
                emit updateProgress(var);
                last_i = var;
            }
        }

        int offset = Config::instance()->buffer_size / 3;
        int just_size = ver.size() / 3;
        if (Config::instance()->buffer_size % 3 != 0) offset += 3;
        for (int f = 0; f < fac->size(); f++) {
            int fs = fac[0][f]->size() / 3;
            for (int i = 0; i < fs; i++)
            {
                stream << "f";
                for (int j = 0; j < 3; j++)
                {
                    stream << " " << fac[0][f]->at(i * 3 + j) + 1 + offset * f;
                }
                stream << endl;
            }
            just_size += fs;
            emit updateProgress(int((just_size) * 100 / sumlen));
        }

    }
    else return false;
    return true;
}

PsiExporter* PsiExporter::inst = NULL;
int PsiExporter::count = 0;

void PsiExporter::clear_instance()
{
    inst = NULL;
}

PsiExporter* PsiExporter::instance()
{
    if (!inst)
    {
        inst = new PsiExporter;
        InputOutputService::abar = new ProgressBar;
    }
    return inst;
}

bool PsiExporter::export_file()
{
    qDebug() << "starting thread " << this;
    QFile file(filename);
    if ( file.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &file );
        stream << "# PSI Format 1.0" << endl;
        stream << "#" << endl;
        stream << "# column[0] = \"x\"" << endl;
        stream << "# column[1] = \"y\"" << endl;
        stream << "# column[2] = \"z\"" << endl;
        stream << "# column[3] = \"Id\"" << endl;
        stream << "# column[4] = \"Chamber\"" << endl;
        stream << "# column[5] = \"Distance\"" << endl;
        stream << "# column[6] = \"Edgable\"" << endl;
        stream << "#" << endl;
        stream << "# symbol[4] = \"C\"" << endl;
        stream << "# symbol[5] = \"D\"" << endl;
        stream << "# symbol[6] = \"E\"" << endl;
        stream << "#" << endl;
        stream << "# type[4] = byte" << endl;
        stream << "# type[5] = float" << endl;
        stream << "# type[6] = byte" << endl;
        stream << endl;
        stream << neighbours.size() << " 2694 115001" << endl;
        stream << "1.00 0.00 0.00" << endl << "0.00 1.00 0.00" << endl << "0.00 0.00 1.00" << endl;
        stream << endl;
        int last_i = 0;
        for (int i = 0; i < neighbours.size(); i++)
        {
            stream << neighbours[i][0] << " " << neighbours[i][1] << " " << neighbours[i][2] << " " << i << " " << neighbours[i][3] << " " << neighbours[i][4] << " " << neighbours[i][5] << endl;
            int var = int(100 * i / neighbours.size());
            if (var != last_i)
            {
                emit updateProgress(var);
                last_i = var;
            }
        }
    }
    else return false;
    return true;
}

InputOutputService::InputOutputService(): config(Config::instance())
{
}

ForamParams InputOutputService::load_parameters(const QString& filename)
{
    ForamParams params;
    QSettings settings(filename, QSettings::IniFormat);

    params.num_of_chambers = settings.value("num_of_chambers", 1).toUInt();
    params.growth_vector_scaling_rate = settings.value("growth_vector_scaling_rate", 0.1).toFloat();
    params.chamber_thickness = settings.value("chamber_thickness", 0.1).toFloat();
    params.deviation_angle = settings.value("deviation_angle", 0).toFloat();
    params.rotation_angle = settings.value("rotation_angle", 0).toFloat();

    GLfloat xrate = settings.value("xrate", 1.050).toFloat();
    GLfloat yrate = settings.value("yrate", 1.050).toFloat();
    GLfloat zrate = settings.value("zrate", 1.0).toFloat();
    params.chamber_scaling_rates = QVector3D(xrate, yrate, zrate);

    int xsize = settings.value("xsize", 32).toInt();
    int ysize = settings.value("ysize", 32).toInt();
    int zsize = settings.value("zsize", 32).toInt();
    params.first_chamber_size = QVector3D(xsize, ysize, zsize);

    return params;
}

void InputOutputService::save_parameters(const QString& filename, ForamParams params)
{
    QSettings settings(filename, QSettings::IniFormat);

    settings.setValue("num_of_chambers", QString::number(params.num_of_chambers));
    settings.setValue("growth_vector_scaling_rate", QString::number(params.growth_vector_scaling_rate));
    settings.setValue("chamber_thickness", QString::number(params.chamber_thickness));
    settings.setValue("rotation_angle", QString::number(params.rotation_angle));
    settings.setValue("deviation_angle", QString::number(params.deviation_angle));

    settings.setValue("xrate", QString::number(params.chamber_scaling_rates.x()));
    settings.setValue("yrate", QString::number(params.chamber_scaling_rates.y()));
    settings.setValue("zrate", QString::number(params.chamber_scaling_rates.z()));

    settings.setValue("xsize", QString::number(params.first_chamber_size.x()));
    settings.setValue("ysize", QString::number(params.first_chamber_size.y()));
    settings.setValue("zsize", QString::number(params.first_chamber_size.z()));

    settings.sync();
}

void InputOutputService::save_amira(const QString& filename, QVector<QVector<float> > neighbours)
{
    PsiExporter::instance()->filename = filename;
    PsiExporter::instance()->neighbours = neighbours;
    PsiExporter::count++;
    connect(PsiExporter::instance(),&QThread::finished, this, &InputOutputService::psi_export_ended);
    PsiExporter::instance()->start();
    connect(PsiExporter::instance(), &PsiExporter::updateProgress, InputOutputService::abar, &ProgressBar::progressChanged);
    InputOutputService::abar->setName(filename);
    InputOutputService::abar->show();
    connect(PsiExporter::instance(),&QThread::finished, InputOutputService::abar, &QObject::deleteLater);
    connect(PsiExporter::instance(),&QThread::finished, PsiExporter::instance(), &QObject::deleteLater);
    PsiExporter::clear_instance();
}

/**
 * @brief Contrabandist::save_obj - exports actual state of vertices and faces to file
 * @param filename - destination file name without extension
 * @param vertices - vector of vertices
 * @param faces - vector of faces (indices)
 */
void InputOutputService::save_obj(const QString& filename, QList<QVector<GLfloat>*>* vertices, QList<QVector<GLushort>*>* faces)
{
    QVector<GLfloat> ver;
    for (int i = 0; i < vertices->size(); i++)
        for (int j = 0; j < vertices->at(i)->size(); j++)
            ver.append(vertices->at(i)->at(j));

    ObjExporter::instance()->filename = filename;
    ObjExporter::instance()->ver = ver;
    ObjExporter::instance()->fac = faces;
    ObjExporter::count++;
    connect(ObjExporter::instance(),&QThread::finished, this, &InputOutputService::obj_export_ended);
    ObjExporter::instance()->bar = new ProgressBar;
    ObjExporter::instance()->start();
    emit obj_started();
    connect(ObjExporter::instance(), &ObjExporter::updateProgress, InputOutputService::obar, &ProgressBar::progressChanged);
    InputOutputService::obar->setName(filename);
    InputOutputService::obar->show();
    connect(ObjExporter::instance(),&QThread::finished, InputOutputService::obar, &QObject::deleteLater);
    connect(ObjExporter::instance(),&QThread::finished, ObjExporter::instance(), &QObject::deleteLater);
    ObjExporter::clear_instance();
}

void InputOutputService::obj_export_ended()
{
    ObjExporter::count--;
    if (ObjExporter::count == 0) emit obj_exports_ended();
}

void InputOutputService::psi_export_ended()
{
    PsiExporter::count--;
}
