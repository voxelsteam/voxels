#ifndef RENDERENGINE_H
#define RENDERENGINE_H

#include <QtOpenGL>
#include <QDebug>
#include <QList>
#include "voxelengine.h"
#include "glwidget.h"
#include "config.h"
#include "camera.h"

#define VAO_BUFFERS 2
#define VERTEX_VBO 0
#define NORMAL_VBO 1

// NVIDIA memory info constants
#define GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX    0x9048
#define GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX  0x9049

// AMD memory info constants
#define VBO_FREE_MEMORY_ATI     0x87FB
#define TEXTURE_FREE_MEMORY_ATI     0x87FC
#define RENDERBUFFER_FREE_MEMORY_ATI    0x87FD

struct Light{
    QVector3D ambient;
    QVector3D diffuse;
    QVector3D specular;
};

class VoxelEngine;
class RenderEngine : protected QOpenGLFunctions
{
public:
    RenderEngine(GLWidget *parent);
    virtual ~RenderEngine();

    void update();
    void render();

    void set_plane(GLfloat x = -1, GLfloat y = -1);
    bool toggle_plane();
    QPushButton* toggle_plane_button = nullptr;

    VoxelEngine* get_voxel_engine() { return this->voxel_engine; }
    Camera* get_camera() { return this->camera; }
    void set_resolution(GLuint width, GLuint height)
    {
        resolution.setX(width);
        resolution.setY(height);

        camera->set_aspect_ratio(width/(GLfloat)height);
    }
    void scale_plane(GLfloat factor) { plane_size *= factor; }

    //Light
    Light *light;

private:
    //initialization functions
    void gather_video_card_data();
    void init_shaders();
    void init_vao();
    void init_background_gradient();

    void render_plane();
    void render_background();
    void destroy_buffers();

    //aggregates
    GLWidget *parent;
    VoxelEngine *voxel_engine;
    Camera *camera;

    GLuint sh_bg_position_attr;
    GLuint sh_bg_color_one_uni;
    GLuint sh_bg_color_second_uni;
    GLuint sh_bg_resolution_uni;

    GLuint sh_position_attr;
    GLuint sh_normals_attr;

    //light uniforms
    GLuint sh_ambient_light_uni;
    GLuint sh_diffuse_light_uni;
    GLuint sh_specular_light_uni;
    GLuint sh_light_direction_uni;

    //material uniforms
    GLuint sh_material_enabled_uni;
    GLuint sh_specular_material_uni;
    GLuint sh_material_shininess_uni;

    //camera uniforms
    GLuint sh_camera_position_uni;
    GLuint sh_transformation_uni;

    //post effects uniforms
    GLuint sh_post_processing_gamma_uni;
    GLuint sh_post_processing_contrast_uni;
    QOpenGLShaderProgram *main_shader;
    QOpenGLShaderProgram *background_shader;

    QOpenGLShaderProgram *plane_shader;
    GLuint sh_planes_position_attr;
    GLuint sh_planes_transformation_uni;

    //VAO
    QOpenGLBuffer *EBO;
    QOpenGLVertexArrayObject *VAO;  
    QList<QOpenGLVertexArrayObject*> *VAO_list;
    QList<GLuint*> *VAO_buffers_list;

    // cutting plane
    QOpenGLVertexArrayObject *plane_VAO;
    QOpenGLBuffer *plane_VBO;
    QMatrix4x4 *plane_transformation;
    GLuint plane_size;
    bool plane_visible = true;
    GLfloat plane_x, plane_y;

    //Background gradient
    QOpenGLVertexArrayObject *background_VAO;
    QOpenGLBuffer *background_VBO;
    QVector3D background_top_gradient_color;
    QVector3D background_bottom_gradient_color;


    //misc variables
    QVector3D base_color;
    GLfloat gamma_level;
    GLfloat contrast_level;
    QVector2D resolution;

    QTime timer;
};

#endif // RENDERENGINE_H
