#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow),
    settingsWindow(NULL)
{
    ui->setupUi(this);
    setWindowTitle("Forams");
    setWindowIcon(QIcon(":icon.ico"));

    QFile File(":qss/stylesheets/" + Config::instance()->stylesheet_name);
    File.open(QFile::ReadOnly);
    QString StyleSheet = QLatin1String(File.readAll());
    qApp->setStyleSheet(StyleSheet);

    //creating dock widgets
    QDockWidget *dockWidget1 = new QDockWidget("SIMULATION");
    dockWidget1->setWidget(ui->sim_wdg);
    configureDockWidget(dockWidget1);
    setTitleLabel("SIMULATION", dockWidget1);
    addDockWidget(Qt::LeftDockWidgetArea, dockWidget1);

    QDockWidget *dockWidget2 = new QDockWidget("MENU");
    dockWidget2->setWidget(ui->ie_wdg);
    configureDockWidget(dockWidget2);
    setTitleLabel("MENU", dockWidget2);
    addDockWidget(Qt::RightDockWidgetArea, dockWidget2);

    QDockWidget *dockWidget3 = new QDockWidget("OUTPUT");
    dockWidget3->setWidget(ui->out_wdg);
    configureDockWidget(dockWidget3);
    setTitleLabel("OUTPUT", dockWidget3);
    addDockWidget(Qt::LeftDockWidgetArea, dockWidget3);

    tabifyDockWidget(dockWidget3, dockWidget1);

    // preparing out_table
    ui->out_table->setRowCount(9);
    ui->out_table->setColumnCount(2);
    ui->out_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->out_table->setColumnWidth(0, 100);

    QFont fnt;
    fnt.setPointSize(8);
    fnt.setFamily("Lucida Sans Unicode");

    QTableWidgetItem *tableLabel1 = new QTableWidgetItem;
    tableLabel1->setFont(fnt);
    tableLabel1->setText("Material volume");
    tableLabel1->setToolTip("Volume of voxels used to build the model");
    ui->out_table->setItem(0,0,tableLabel1);

    QTableWidgetItem *tableLabel2 = new QTableWidgetItem;
    tableLabel2->setFont(fnt);
    tableLabel2->setText("Total volume");
    tableLabel2->setToolTip("Volume of voxels used to build model and space inside chambers");
    ui->out_table->setItem(1,0,tableLabel2);

    QTableWidgetItem *tableLabel3 = new QTableWidgetItem;
    tableLabel3->setFont(fnt);
    tableLabel3->setText("Surface area");
    tableLabel3->setToolTip("Surface area of all generated model");
    ui->out_table->setItem(2,0,tableLabel3);

    QTableWidgetItem *tableLabel4 = new QTableWidgetItem;
    tableLabel4->setFont(fnt);
    tableLabel4->setText("L-center");
    tableLabel4->setToolTip("Sum of distances between centres of every each following chamber");
    ui->out_table->setItem(3,0,tableLabel4);

    QTableWidgetItem *tableLabel5 = new QTableWidgetItem;
    tableLabel5->setFont(fnt);
    tableLabel5->setText("L-aperture");
    tableLabel5->setToolTip("Sum of distances between apertures of every each following chamber");
    ui->out_table->setItem(4,0,tableLabel5);

    QTableWidgetItem *tableLabel6 = new QTableWidgetItem;
    tableLabel6->setFont(fnt);
    tableLabel6->setText("L-center'");
    tableLabel6->setToolTip("Distance between centres of the first and the last chamber");
    ui->out_table->setItem(5,0,tableLabel6);

    QTableWidgetItem *tableLabel7 = new QTableWidgetItem;
    tableLabel7->setFont(fnt);
    tableLabel7->setText("L-aperture'");
    tableLabel7->setToolTip("Distance between apertures of the first and the last chamber");
    ui->out_table->setItem(6,0,tableLabel7);

    QTableWidgetItem *tableLabel8 = new QTableWidgetItem;
    tableLabel8->setFont(fnt);
    tableLabel8->setText("N-center");
    tableLabel8->setToolTip("Ratio of L-center to L-center'");
    ui->out_table->setItem(7,0,tableLabel8);

    QTableWidgetItem *tableLabel9 = new QTableWidgetItem;
    tableLabel9->setFont(fnt);
    tableLabel9->setText("N-aperture");
    tableLabel9->setToolTip("Ratio of L-aperture to L-aperture'");
    ui->out_table->setItem(8,0,tableLabel9);

    ui->NextButton->setDisabled(true);
    ui->PrevButton->setDisabled(true);

    ui->voxelsDivButton->setDisabled(true);
    ui->voxelsMulButton->setDisabled(true);

    connected = false;

    ui->intersectButton->setDisabled(true);
    ui->intersectNegativeButton->setDisabled(true);
    ui->ExportButton->setDisabled(true);
    ui->intersectResetButton->setDisabled(true);
    ui->ResetButton->setDisabled(true);
    ui->PrintScreenButton->setDisabled(true);
    ui->planeToggleButton->setDisabled(true);
}

void MainWindow::showEvent(QShowEvent *e)
{
    QMainWindow::showEvent(e);
    static bool firstStart = true;
    if (firstStart)
    {
        ui->openGLWidget->get_scene()->render_engine->toggle_plane_button = ui->planeToggleButton;

        // generate default plane
        ui->openGLWidget->get_scene()->render_engine->set_plane(0, ui->openGLWidget->height()/2);
        ui->openGLWidget->get_scene()->render_engine->toggle_plane();

        firstStart = false;
    }
}

/**
 * @brief MainWindow::configureDockWidget - sets widget's allowed areas, flags and features
 * @param dockWidget - dock widget which all parameters will be set
 */
void MainWindow::configureDockWidget(QDockWidget* dockWidget)
{
    dockWidget->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    dockWidget->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);
    dockWidget->setFeatures( QDockWidget::DockWidgetFloatable| QDockWidget::DockWidgetMovable);
    dockWidget->setParent(this);
}

/**
 * @brief MainWindow::setTitleLabel - sets dock widget's name on title bar and configure font using QLabel
 * @param labelName - name to set
 * @param dockWidget - dock widget which label will be set
 */
void MainWindow::setTitleLabel(const QString& labelName, QDockWidget* dockWidget)
{
    QLabel *label = new QLabel(labelName, dockWidget);
    label->setStyleSheet("font-size: 10pt; font-weight: bold; margin: 3px;");
    label->setAlignment(Qt::AlignHCenter);
    dockWidget->setTitleBarWidget(label);
}

void MainWindow::fillOutputTable()
{
    QFont fnt;
    fnt.setPointSize(8);
    fnt.setFamily("Lucida Sans Unicode");

    QTableWidgetItem *qtitem0 = new QTableWidgetItem;
    qtitem0->setText(QString::number(ui->openGLWidget->get_scene()->foraminifera->get_material_volume()) + QString("µm\u00B3"));
    qtitem0->setFont(fnt);
    ui->out_table->setItem(0,1,qtitem0);

    QTableWidgetItem *qtitem1 = new QTableWidgetItem;
    qtitem1->setText(QString::number(ui->openGLWidget->get_scene()->foraminifera->get_total_volume())+ QString("µm\u00B3"));
    qtitem1->setFont(fnt);
    ui->out_table->setItem(1,1,qtitem1);

    QTableWidgetItem *qtitem2 = new QTableWidgetItem;
    qtitem2->setText(QString::number(ui->openGLWidget->get_scene()->foraminifera->get_surface_area())+ QString("µm\u00B2"));
    qtitem2->setFont(fnt);
    ui->out_table->setItem(2,1,qtitem2);

    QTableWidgetItem *qtitem3 = new QTableWidgetItem;
    qtitem3->setText(QString::number(ui->openGLWidget->get_scene()->foraminifera->l_center, 'f', 2 )+ QString("µm"));
    qtitem3->setFont(fnt);
    ui->out_table->setItem(3,1,qtitem3);

    QTableWidgetItem *qtitem4 = new QTableWidgetItem;
    qtitem4->setText(QString::number(ui->openGLWidget->get_scene()->foraminifera->l_aperture, 'f', 2 )+ QString("µm"));
    qtitem4->setFont(fnt);
    ui->out_table->setItem(4,1,qtitem4);

    QTableWidgetItem *qtitem5 = new QTableWidgetItem;
    qtitem5->setText(QString::number(ui->openGLWidget->get_scene()->foraminifera->l_center_prime,'f', 2 )+ QString("µm"));
    qtitem5->setFont(fnt);
    ui->out_table->setItem(5,1,qtitem5);

    QTableWidgetItem *qtitem6 = new QTableWidgetItem;
    qtitem6->setText(QString::number(ui->openGLWidget->get_scene()->foraminifera->l_aperture_prime,'f', 2 )+ QString("µm"));
    qtitem6->setFont(fnt);
    ui->out_table->setItem(6,1,qtitem6);

    QTableWidgetItem *qtitem7 = new QTableWidgetItem;
    qtitem7->setText(QString::number(ui->openGLWidget->get_scene()->foraminifera->get_n_center(), 'f', 2 ) );
    qtitem7->setFont(fnt);
    ui->out_table->setItem(7,1,qtitem7);

    QTableWidgetItem *qtitem8 = new QTableWidgetItem;
    qtitem8->setText(QString::number(ui->openGLWidget->get_scene()->foraminifera->get_n_aperture(), 'f', 2 ));
    qtitem8->setFont(fnt);
    ui->out_table->setItem(8,1,qtitem8);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void  MainWindow::closeEvent(QCloseEvent* event)
{
    Config::instance()->saveConfig();
    if (ObjExporter::count > 0 || PsiExporter::count > 0)
    {
        QMessageBox::StandardButton resBtn = QMessageBox::question( this, "Are you sure?",
                                                                        tr("Are you sure? Detected unfinished exports will be broken.\n"),
                                                                        QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
                                                                        QMessageBox::Yes);
            if (resBtn != QMessageBox::Yes) {
                event->ignore();
            } else {
                qApp->quit();
            }
    } else qApp->quit();
}

void MainWindow::simulate()
{

    ForamParams params;
    params.first_chamber_size.setX(ui->x_size->value());
    params.first_chamber_size.setY(ui->y_size->value());
    params.first_chamber_size.setZ(ui->z_size->value());

    params.chamber_scaling_rates.setX(ui->k_x->value());
    params.chamber_scaling_rates.setY(ui->k_y->value());
    params.chamber_scaling_rates.setZ(ui->k_z->value());

    params.deviation_angle = ui->deviationAngle->value();
    params.rotation_angle = ui->rotationAngle->value();

    params.num_of_chambers = ui->num_of_chambs->value();
    params.chamber_thickness = ui->chamb_thick->value();
    params.growth_vector_scaling_rate = ui->growth_scaling_rate->value();

    ui->openGLWidget->makeCurrent();
    ui->openGLWidget->get_scene()->setup_scene(params);
    ui->openGLWidget->doneCurrent();

    ui->stepCounter->setText(QString::number(params.num_of_chambers));
    ui->PrevButton->setDisabled(false);
    ui->NextButton->setDisabled(true);
    if (params.num_of_chambers == 1) ui->PrevButton->setDisabled(true);
}

void MainWindow::on_SimuateButton_clicked()
{
    ui->openGLWidget->get_scene()->reset_voxels_multiplier();
    ui->multiplier->setText("1");
    ui->voxelsDivButton->setDisabled(false);
    ui->voxelsMulButton->setDisabled(false);
    ui->intersectButton->setDisabled(false);
    ui->intersectNegativeButton->setDisabled(false);
    ui->ExportButton->setDisabled(false);
    ui->intersectResetButton->setDisabled(false);
    ui->ResetButton->setDisabled(false);
    ui->PrintScreenButton->setDisabled(false);
    ui->planeToggleButton->setDisabled(false);
    simulate();

    fillOutputTable();
}

void MainWindow::on_PrevButton_clicked()
{
    ui->openGLWidget->makeCurrent();
    ui->NextButton->setDisabled(false);
    if (ui->openGLWidget->get_scene()->foraminifera->visualize_prev_step())
    {
        ui->PrevButton->setDisabled(true);
    }
    ui->stepCounter->setText(QString::number(ui->openGLWidget->get_scene()->foraminifera->visualized_step));

    ui->openGLWidget->get_scene()->render_engine->update();
    ui->openGLWidget->update();
    ui->openGLWidget->doneCurrent();
}

void MainWindow::on_NextButton_clicked()
{
    ui->openGLWidget->makeCurrent();
    ui->PrevButton->setDisabled(false);
    if(ui->openGLWidget->get_scene()->foraminifera->visualize_next_step())
    {
        ui->NextButton->setDisabled(true);
    }
    ui->stepCounter->setText(QString::number(ui->openGLWidget->get_scene()->foraminifera->visualized_step));

    ui->openGLWidget->get_scene()->render_engine->update();
    ui->openGLWidget->update();
    ui->openGLWidget->doneCurrent();
}

void MainWindow::on_ImportButton_clicked()
{
    if (!connected)
    {
        connect(ui->openGLWidget->get_scene()->get_objHandler(), &InputOutputService::obj_started, this, &MainWindow::export_in_progress);
        connect(ui->openGLWidget->get_scene()->get_objHandler(), &InputOutputService::obj_exports_ended, this, &MainWindow::export_ended);
        connected = true;
    }

    QString name  = QFileDialog::getOpenFileName(this, tr("Choose file to import."), "",  "Supported files (*.ini)", 0, 0);
    if(!name.isEmpty())
    {
        if(!name.endsWith((".ini"))) name += ".ini";

        ForamParams params = ui->openGLWidget->get_scene()->load_parameters(name);
        ui->chamb_thick->setValue(params.chamber_thickness);

        ui->k_x->setValue(params.chamber_scaling_rates.x());
        ui->k_y->setValue(params.chamber_scaling_rates.y());
        ui->k_z->setValue(params.chamber_scaling_rates.z());

        ui->x_size->setValue(params.first_chamber_size.x());
        ui->y_size->setValue(params.first_chamber_size.y());
        ui->z_size->setValue(params.first_chamber_size.z());

        ui->growth_scaling_rate->setValue(params.growth_vector_scaling_rate);
        ui->num_of_chambs->setValue(params.num_of_chambers);

        ui->openGLWidget->makeCurrent();

        ui->openGLWidget->get_scene()->setup_scene(params);
        fillOutputTable();

        ui->openGLWidget->doneCurrent();
    }
}

void MainWindow::on_ExportButton_clicked()
{
    if (!connected)
    {
        connect(ui->openGLWidget->get_scene()->get_objHandler(), &InputOutputService::obj_started, this, &MainWindow::export_in_progress);
        connect(ui->openGLWidget->get_scene()->get_objHandler(), &InputOutputService::obj_exports_ended, this, &MainWindow::export_ended);
        connected = true;
    }
    QString default_file_name = "foram-" + QDateTime::currentDateTime().toString("dd-MM-yyyy_hh-mm-ss");
    QString selected_filter;

    QString name = QFileDialog::getSaveFileName(this, tr("Choose destination."), default_file_name,  "3D model (*.obj);;Amira PSI (*.psi);;Foraminifera parameters (*.ini)", &selected_filter, 0);
    if(!name.isEmpty()) {
        if(selected_filter == "3D model (*.obj)")
        {
            if(!name.endsWith((".obj"))) name += ".obj";
            ui->openGLWidget->get_scene()->save_obj(name);
        }
        else if(selected_filter == "Amira PSI (*.psi)")
        {
            if(!name.endsWith((".psi"))) name += ".psi";
            ui->openGLWidget->get_scene()->save_amira(name);
        }
        else
        {
            if(!name.endsWith((".ini"))) name += ".ini";
            ui->openGLWidget->get_scene()->save_parameters(name);
        }
    }
}

void MainWindow::on_ResetButton_clicked()
{
    ui->openGLWidget->get_scene()->get_camera()->reset();
    ui->openGLWidget->update();
}

void MainWindow::on_PrintScreenButton_clicked()
{
    ui->openGLWidget->screenshot();
}

void MainWindow::on_intersectButton_clicked()
{
    ui->openGLWidget->get_scene()->intersect(false);
}

void MainWindow::on_intersectNegativeButton_clicked()
{
    ui->openGLWidget->get_scene()->intersect(true);
}

void MainWindow::on_intersectResetButton_clicked()
{
    ui->openGLWidget->get_scene()->reset();
}

void MainWindow::on_planeToggleButton_clicked()
{
    QString button_text;
    ui->openGLWidget->get_scene()->render_engine->toggle_plane() ? button_text = "Hide plane" : button_text = "Show plane";
    ui->planeToggleButton->setText(button_text);
}

void MainWindow::on_voxelsDivButton_clicked()
{
    if(ui->openGLWidget->get_scene()->div_voxels())
    {
        simulate();
        ui->multiplier->setText(QString::number(ui->openGLWidget->get_scene()->voxels_multiplier));
        ui->voxelsMulButton->setDisabled(false);
        if(ui->openGLWidget->get_scene()->voxels_multiplier == 0.5f){
            ui->voxelsDivButton->setDisabled(true);
        }
    }
}

void MainWindow::on_voxelsMulButton_clicked()
{
    if(ui->openGLWidget->get_scene()->mul_voxels())
    {
        simulate();
        ui->multiplier->setText(QString::number(ui->openGLWidget->get_scene()->voxels_multiplier));
        ui->voxelsDivButton->setDisabled(false);
        if(ui->openGLWidget->get_scene()->voxels_multiplier == 2){
            ui->voxelsMulButton->setDisabled(true);
        }
    }
}

void MainWindow::on_aboutButton_clicked()
{
    QFont fnt;
    fnt.setPointSize(8);
    fnt.setFamily("Lucida Sans Unicode");

    QMessageBox msgBox;
    msgBox.setWindowFlags(Qt::CustomizeWindowHint);
    msgBox.setFont(fnt);
    QString text = "<p align='center'>"
                   "Authors:<br>"
                   "Andrzej Czerniewski, Michał Furmanek, Norbert Skurnóg, Katarzyna Staszczak<br>"
                   "dr inż. Paweł Topa"
                   "</p>";
    msgBox.setText(text);
    msgBox.setStandardButtons(QMessageBox::Close);
    msgBox.exec();
}

void MainWindow::on_pushButton_clicked()
{
    QFont fnt;
    fnt.setPointSize(8);
    fnt.setFamily("Lucida Sans Unicode");

    QMessageBox msgBox;
    msgBox.setWindowFlags(Qt::CustomizeWindowHint);
    msgBox.setFont(fnt);
    QString text = ""
                   "<p align='left'>"
                   "Useful mouse actions:<br>"
                   "scroll up - move model closer<br>"
                   "scroll down - move model further<br>"
                   "left mouse button pressed and move left/right - move model left/right<br>"
                   "left mouse button pressed and move up/down- move model up/down<br>"
                   "right mouse button pressed and move left/right - turn model right/left<br>"
                   "right mouse button pressed and move up/down - turn model down/up<br>"
                   "center mouse button pressed (under the wheel) and slide up/down - turn camera up/down<br>"
                   "pressed shift and mouse move allows setting intersection plane"
                   "</p>";
    msgBox.setText(text);
    msgBox.setStandardButtons(QMessageBox::Close);
    msgBox.exec();
}

void MainWindow::on_settingsButton_clicked()
{
    settingsWindow = new SettingsWindow(ui->openGLWidget);
    connect(settingsWindow, SIGNAL(updateMarchingCubesState()), this, SLOT(on_SimuateButton_clicked()));
    settingsWindow->show();

}

void MainWindow::export_in_progress()
{
    ui->ImportButton->setDisabled(true);
    ui->SimuateButton->setDisabled(true);
    ui->intersectButton->setDisabled(true);
    ui->intersectNegativeButton->setDisabled(true);
    ui->intersectResetButton->setDisabled(true);
    ui->voxelsDivButton->setDisabled(true);
    ui->voxelsMulButton->setDisabled(true);
    ui->NextButton->setDisabled(true);
    ui->PrevButton->setDisabled(true);
}

void MainWindow::export_ended()
{
    ui->ImportButton->setDisabled(false);
    ui->SimuateButton->setDisabled(false);
    ui->intersectButton->setDisabled(false);
    ui->intersectNegativeButton->setDisabled(false);
    ui->intersectResetButton->setDisabled(false);
    ui->voxelsDivButton->setDisabled(false);
    ui->voxelsMulButton->setDisabled(false);
    ui->NextButton->setDisabled(false);
    ui->PrevButton->setDisabled(false);
}
