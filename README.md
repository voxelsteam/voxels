# README #

Wykorzystanie techniki voxeli dla potrzeb wizualizacji generowanych komputerowo form przestrzennych.

### Dokumentacja ###
[Diagram klas](https://drive.google.com/file/d/0B9jY3a_JmAh6bnIyWnBzcU5UTVE/view?usp=sharing)

[Dokumentacja procesowa](https://docs.google.com/document/d/1LdT3ITrPchVI44t40ITbVIWzXnCPdPdYCmlvHChB5GY)   
[Dokumentacja techniczna](https://docs.google.com/document/d/1MG7VVfbDfW2zTOReBZVX78c9uZ0TaI2uEJii1Ia4kUk)   
[Podręcznik użytkownika](https://docs.google.com/document/d/1zauRf2pE0-B4AaxD2-BOA2YBXjHDFjYCn--kXraDEtM/edit?pli=1)   
[Przewodnik po pracy](https://docs.google.com/document/d/1iRSLZ9E3OMIOr4AiTea8TE9QH0Mf2Fk47WT6woESgGA/edit?pref=2&pli=1#)   
   
[Notatki ze spotkań](https://docs.google.com/document/d/161E3ykcDGezN38nqs82wJMeIidRmyslHt7NA4wh0MFU/edit?pli=1)  

### Useful links ###
http://www.ing.pan.pl/str_prac/Tyszka_J/2Labaj_etal_2003.pdf   
http://pl.wikipedia.org/wiki/Otwornice  
[Markdown](http://en.wikipedia.org/wiki/Markdown)  
[Google C++ Style Guide](https://google-styleguide.googlecode.com/svn/trunk/cppguide.html)  
[Dobry tutorial do OpenGL](http://learnopengl.com/#!Introduction)

### Pozostale ###
[Prezentacja projektu](https://prezi.com/vh12k6a9quoj/wykorzystanie-techniki-voxeli-dla-potrzeb-wizualizacji-gener/)