#include "config.h"

Config* Config::inst = NULL;

Config* Config::instance()
{
    if (!inst) inst = new Config;
    return inst;
}

Config::Config()
{
    QFileInfo file_info(CONFIG_FILE_NAME);
    settings = new QSettings(CONFIG_FILE_NAME, QSettings::IniFormat);

    screen_dir = settings->value("screen_dir", "screenshots").toString();
    screen_core = settings->value("screen_core", "shot").toString();
    export_dir = settings->value("export_dir", "exported").toString();
    gamma_level = settings->value("gamma_level", 1).toFloat();
    contrast_level = settings->value("contrast_level", 2).toFloat();
    export_filetype = settings->value("export_filetype", -1).toInt();

    world_size = settings->value("world_size", 1024).toInt();

    z_near = settings->value("z_near", 0.1f).toFloat();
    z_far = settings->value("z_far", 10000.0f).toFloat();
    fov = settings->value("fov", 0.610865238f).toFloat();
    start_camera_distance = settings->value("start_camera_distance", 50).toFloat();
    plane_size = settings->value("plane_size", 512).toInt();

    x_mouse_sens = settings->value("x_mouse_sens", 1.0f).toFloat();
    y_mouse_sens = settings->value("y_mouse_sens", 1.0f).toFloat();
    wheel_sens = settings->value("wheel_sens", 0.001f).toFloat();

    buffer_size = settings->value("buffer_size", 100000).toLongLong();

    stylesheet_name = settings->value("stylesheet_name", "default.qss").toString();
    bottom_gradient_color = settings->value("bottom_gradient_color", "44,62,80").toString();
    top_gradient_color = settings->value("top_gradient_color", "41,128,185").toString();

    if(!file_info.exists()) saveConfig();
}

void Config::saveConfig()
{
    settings->setValue("screen_dir", screen_dir);
    settings->setValue("screen_core", screen_core);
    settings->setValue("export_dir", export_dir);
    settings->setValue("gamma_level", QString::number(gamma_level));
    settings->setValue("contrast_level", QString::number(contrast_level));

    settings->setValue("world_size", QString::number(world_size));

    settings->setValue("z_near", QString::number(z_near));
    settings->setValue("z_far", QString::number(z_far));
    settings->setValue("fov", QString::number(fov));

    settings->setValue("x_mouse_sens", QString::number(x_mouse_sens));
    settings->setValue("y_mouse_sens", QString::number(y_mouse_sens));
    settings->setValue("wheel_sens", QString::number(wheel_sens));

    settings->setValue("z_near", QString::number(z_near));
    settings->setValue("z_far", QString::number(z_far));
    settings->setValue("fov", QString::number(fov));
    settings->setValue("start_camera_distance", QString::number(start_camera_distance));
    settings->setValue("plane_size", QString::number(plane_size));

    settings->setValue("x_mouse_sens", QString::number(x_mouse_sens));
    settings->setValue("y_mouse_sens", QString::number(y_mouse_sens));
    settings->setValue("wheel_sens", QString::number(wheel_sens));

    settings->setValue("buffer_size", QString::number(buffer_size));

    settings->setValue("stylesheet_name", stylesheet_name);
    settings->setValue("bottom_gradient_color", bottom_gradient_color);
    settings->setValue("top_gradient_color", top_gradient_color);

    settings->sync();
}
