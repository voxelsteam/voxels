#ifndef INPUTOUTPUTSERVICE
#define INPUTOUTPUTSERVICE

#include <QtOpenGL>
#include <QVector>
#include <QSettings>
#include "glwidget.h"
#include "config.h"
#include "foraminifera.h"
#include "progressbar.h"

class ObjExporter : public QThread
{
    Q_OBJECT
public:
    QList<QVector<GLushort>*>* fac;
    QVector<GLfloat> ver;
    QString filename;
    bool export_file();
    static ObjExporter* instance();
    static void clear_instance();
    static int count;
    ProgressBar* bar;

signals:
    void updateProgress(int value);

private:
    static ObjExporter* inst;
    void run()
    {
        if (export_file()) qDebug() << "Plik wyeksportowany pomyślnie";
        else qDebug() << "Eksportowanie pliku nie powiodło się";
    }
};

class PsiExporter : public QThread
{
    Q_OBJECT
public:
    QString filename;
    QVector<QVector<float> > neighbours;
    Foraminifera* foraminifera;
    bool export_file();
    static PsiExporter* instance();
    static void clear_instance();
    static int count;
    ProgressBar* bar;

signals:
    void updateProgress(int value);

private:
    static PsiExporter* inst;
    void run()
    {
        if (export_file()) qDebug() << "Plik wyeksportowany pomyślnie";
        else qDebug() << "Eksportowanie pliku nie powiodło się";
    }
};

class InputOutputService : public QObject
{
    Q_OBJECT

public:
    InputOutputService();

    ForamParams load_parameters(const QString& filename);
    static ProgressBar* obar;
    static ProgressBar* abar;

    void save_parameters(const QString& filename, ForamParams params);
    void save_amira(const QString& filename, QVector<QVector<float>> neighbours);
    void save_obj(const QString& filename, QList<QVector<GLfloat>*>* vertices, QList<QVector<GLushort>*>* faces);

public slots:
    void obj_export_ended();
    void psi_export_ended();

signals:
    void obj_started();
    void obj_exports_ended();

private:
    Config* config;
};

#endif // INPUTOUTPUTSERVICE
