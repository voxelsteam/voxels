#ifndef FORAMINIFERA_H
#define FORAMINIFERA_H
#include <QDebug>
#include <QtOpenGL>
#include <limits>
#include <cmath>
#include <ctime>

#include "config.h"
#include "voxelengine.h"
#include "sparsematrix3d.h"

#define LOCAL_MINIMALIZATION 1
#define CLOSEST_TO_PREVIOUS_APERTURE 2

struct ForamParams
{
    GLuint num_of_chambers;
    GLfloat growth_vector_scaling_rate;
    QVector3D chamber_scaling_rates;
    GLfloat chamber_thickness;
    QVector3D first_chamber_size;
    GLfloat rotation_angle;
    GLfloat deviation_angle;
};

class Foraminifera
{
public:
    Foraminifera(const ForamParams& params, GLfloat voxels_multiplier, VoxelEngine* voxel_engine);
    virtual ~Foraminifera();

    //main functions
    void add_foraminifera_to_world(GLuint till_step = 0);

    bool visualize_next_step();
    bool visualize_prev_step();
    QVector<QVector<float>> stepped_voxels();

    //functions for calculating additional parameters
    GLfloat get_n_center();
    GLfloat get_n_aperture();
    QVector3D get_min_rect_point_A();
    QVector3D get_min_rect_point_B();

    int get_material_volume();
    int get_total_volume() { return this->total_volume; }
    int get_surface_area();

    ForamParams& get_params() { return this->params; }

    //additional parameters that have to be calculated
    GLfloat l_center = 0.0f;
    GLfloat l_aperture = 0.0f;
    GLfloat l_center_prime = 0.0f;
    GLfloat l_aperture_prime = 0.0f;

    GLuint visualized_step = 0;

private:
    VoxelEngine *voxel_engine;

    GLfloat voxels_multiplier;
    QVector<QVector3D> centers;

    ForamParams params;

    void generate_foraminifera();
    void visualize_step(GLuint step);

    //other variables
    GLint WORLD_SIZE;

    SparseMatrix3D* filled_spaces;

    int current_step = 0;
    QVector3D current_aperture;
    QVector3D prev_aperture;

    int total_volume = 0;

    //voxels that will be returned to voxel engine
    QVector<QVector<QVector3D > > foraminifera_voxels;
    QVector<QVector<QVector3D > > foraminifera_aperture_holes;

    //algorithm helper functions
    QVector3D generate_chamber(GLuint chamber_num, const QVector3D center, const QVector3D radii, const GLuint chamber_thickness, int aperture_finding_method);
    QVector3D deviate_and_rotate(float deviation_angle, float rotation_angle);

    //math helper functions
    QVector3D rotate_around_arbitrary_line(QVector3D vec, QVector3D axis_point_one, QVector3D axis_point_two, float angle);

    int count_free_walls(int x, int y, int z);

    QTime timer;
};

#endif // FORAMINIFERA_H
