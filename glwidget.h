#ifndef GLWINDOW_H
#define GLWINDOW_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QtOpenGL>
#include "config.h"

class Scene;
class Camera;
class GLWidget: public QOpenGLWidget, public QOpenGLFunctions
{
    Q_OBJECT

public:
    GLWidget(QWidget *parent=0);
    ~GLWidget();

    void screenshot();

    //debug
    Scene* get_scene() { return this->scene; }

protected:
    void initializeGL() Q_DECL_OVERRIDE;
    void paintGL() Q_DECL_OVERRIDE;

    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void wheelEvent (QWheelEvent *event) Q_DECL_OVERRIDE;

private:
    GLfloat x_mouse_sens, y_mouse_sens;
    GLfloat wheel_sens;
    QPoint last_mouse_pos;

    QString date_format;
    QString screen_dir;
    QString screen_core;

    GLfloat aspect_ratio;

    GLushort num_of_samples;

    Scene *scene;
    Camera *camera;
    GLfloat normalize_angle(GLfloat angle);
};

#endif // GLWINDOW_H
