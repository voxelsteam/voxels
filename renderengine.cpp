#include "renderengine.h"

RenderEngine::RenderEngine(GLWidget *_parent) :
    parent(_parent), plane_size(Config::instance()->plane_size),
    gamma_level(Config::instance()->gamma_level), contrast_level(Config::instance()->contrast_level)
{    
    initializeOpenGLFunctions();
    gather_video_card_data();

    // pre-render settings
    glClearColor(25.0f/256.0f, 25.0f/256.0f, 25.0f/256.0f, 0);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_MULTISAMPLE);

    main_shader = new QOpenGLShaderProgram(parent);
    plane_shader = new QOpenGLShaderProgram(parent);
    background_shader = new QOpenGLShaderProgram(parent);

    VAO_list = new QList<QOpenGLVertexArrayObject*>();
    VAO_buffers_list = new QList<GLuint*>();

    plane_VAO = new QOpenGLVertexArrayObject();
    plane_VAO->create();
    plane_VBO = new QOpenGLBuffer;
    plane_VBO->create();
    plane_transformation = new QMatrix4x4;

    background_VAO = new QOpenGLVertexArrayObject();
    background_VAO->create();
    background_VBO = new QOpenGLBuffer;
    background_VBO->create();
    background_top_gradient_color = QVector3D(Config::instance()->top_gradient_color.split(",")[0].toFloat()/256.0, Config::instance()->top_gradient_color.split(",")[1].toFloat()/256.0, Config::instance()->top_gradient_color.split(",")[2].toFloat()/256.0);
    background_bottom_gradient_color = QVector3D(Config::instance()->bottom_gradient_color.split(",")[0].toFloat()/256.0,   Config::instance()->bottom_gradient_color.split(",")[1].toFloat()/256.0,   Config::instance()->bottom_gradient_color.split(",")[2].toFloat()/256.0);

    light = new Light;
    light->ambient = QVector3D(1.0f, 1.0f, 1.0f) * 0.5f;
    light->diffuse = QVector3D(1.0f, 1.0f, 1.0f) * 0.45f;
    light->specular = QVector3D(1.0f, 1.0f, 1.0f) * 0.05f;

    EBO = new QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
    EBO->create();

    camera = new Camera;

    voxel_engine = new VoxelEngine();

    init_shaders();
    init_background_gradient();
}

RenderEngine::~RenderEngine()
{
    delete camera;
    camera = nullptr;

    delete voxel_engine;
    voxel_engine = nullptr;

    main_shader->release();
    delete main_shader;
    main_shader = nullptr;

    plane_shader->release();
    delete plane_shader;
    plane_shader = nullptr;

    background_shader->release();
    delete background_shader;
    background_shader = nullptr;

    delete light;
    light = nullptr;

    destroy_buffers();
    delete VAO_list;
    VAO_list = nullptr;
    delete VAO_buffers_list;
    VAO_buffers_list = nullptr;

    delete plane_VAO;
    plane_VAO = nullptr;

    delete plane_VBO;
    plane_VBO = nullptr;

    delete background_VBO;
    background_VBO = nullptr;

    delete background_VAO;
    background_VAO = nullptr;

    delete EBO;
    EBO = nullptr;
}

void RenderEngine::destroy_buffers()
{
    for(GLuint *VBOs: *VAO_buffers_list) {
//        glDeleteBuffers(VAO_BUFFERS, VBOs);
        delete VBOs;
        VBOs = nullptr;
    }
    VAO_buffers_list->clear();

    for(QOpenGLVertexArrayObject *VAO: *VAO_list) {
        VAO->destroy();
        delete VAO;
        VAO = nullptr;
    }
    VAO_list->clear();
}

/**
 * Collects and prints video card data
 * @brief RenderEngine::gather_video_card_data
 */
void RenderEngine::gather_video_card_data()
{
    qDebug() << "[VIDEO CARD INFO]";
    qDebug() << "Vendor: " << (const char*)glGetString(GL_VENDOR);
    qDebug() << "GPU: " << (const char*)glGetString(GL_RENDERER);
    qDebug() << "GL Version: " << (const char*)glGetString(GL_VERSION);
    qDebug() << "GLSL Version: " << (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION);

    // NVIDIA
    GLint total_memory, available_memory;
    glGetError();

    glGetIntegerv(GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX, &total_memory);
    glGetIntegerv(GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX, &available_memory);

    if(glGetError() == GL_NO_ERROR) {
        qDebug("Available VRAM: %d/%d MB\n", available_memory/1024, total_memory/1024);
    }
    else
    {
        // AMD
        GLint vbo_free, texture_free, renderbuffer_free;
        glGetError();

        glGetIntegerv(VBO_FREE_MEMORY_ATI, &vbo_free);
        glGetIntegerv(TEXTURE_FREE_MEMORY_ATI, &texture_free);
        glGetIntegerv(RENDERBUFFER_FREE_MEMORY_ATI, &renderbuffer_free);

        if(glGetError() == GL_NO_ERROR)
        {
            qDebug("Available memory: VBO %d, textures %d, render buffer %d\n", vbo_free, texture_free, renderbuffer_free);
        }
        else
        {
            qDebug("\n");
        }
    }
}

/**
 * @brief RenderEngine::init_shaders shader initialization
 */
void RenderEngine::init_shaders()
{
    timer.start();

    main_shader->addShaderFromSourceFile(QOpenGLShader::Vertex, ":shaders/main.vert");
    main_shader->addShaderFromSourceFile(QOpenGLShader::Fragment, ":shaders/main.frag");
    main_shader->link();

    //shader attributes
    sh_position_attr = main_shader->attributeLocation("posAttr");
    sh_normals_attr = main_shader->attributeLocation("normAttr");

    // shader uniforms
    sh_transformation_uni = main_shader->uniformLocation("transformation");
    sh_camera_position_uni = main_shader->uniformLocation("cameraPosition");
    sh_post_processing_gamma_uni = main_shader->uniformLocation("gamma");
    sh_post_processing_contrast_uni = main_shader->uniformLocation("contrast");

    //light properties
    sh_ambient_light_uni = main_shader->uniformLocation("light.ambient");
    sh_diffuse_light_uni = main_shader->uniformLocation("light.diffuse");
    sh_specular_light_uni = main_shader->uniformLocation("light.specular");
    sh_material_shininess_uni = main_shader->uniformLocation("shininess");

    background_shader->addShaderFromSourceFile(QOpenGLShader::Vertex, ":shaders/background.vert");
    background_shader->addShaderFromSourceFile(QOpenGLShader::Fragment, ":shaders/background.frag");
    background_shader->link();

    //background attributes
    sh_bg_position_attr = background_shader->attributeLocation("posAttr");
    sh_bg_color_one_uni = background_shader->uniformLocation("topColor");
    sh_bg_color_second_uni = background_shader->uniformLocation("bottomColor");
    sh_bg_resolution_uni = background_shader->uniformLocation("resolution");

    // planes shader
    plane_shader->addShaderFromSourceFile(QOpenGLShader::Vertex, ":shaders/plane.vert");
    plane_shader->addShaderFromSourceFile(QOpenGLShader::Fragment, ":shaders/plane.frag");
    plane_shader->link();

    sh_planes_position_attr = plane_shader->attributeLocation("posAttr");
    sh_planes_transformation_uni = plane_shader->uniformLocation("transformation");

    qDebug("[RENDER ENGINE] Shaders initialized [%d ms]", timer.elapsed());
}

/**
 * @brief RenderEngine::init_vao initializes geometry, if already initialized, reinitializes
 */
void RenderEngine::init_vao()
{
    timer.start();  

    // clear old buffers (if any) before generating new ones
    destroy_buffers();

    // generate new VBOs and VAOs
    GLushort vertex_buffer_size = this->voxel_engine->vertices_list->size();
    for(int i = 0; i < vertex_buffer_size; i++)
    {
        VAO = new QOpenGLVertexArrayObject();
        VAO->create();
        VAO->bind();

        QVector<GLfloat>* vertices = this->voxel_engine->vertices_list->at(i);
        QVector<GLfloat>* normals = this->voxel_engine->normals_list->at(i);

        QOpenGLBuffer vertices_VBO, normals_VBO;
        GLuint *VBO_ids = new GLuint[VAO_BUFFERS];

        vertices_VBO.create();
        vertices_VBO.bind();
        glBufferData(GL_ARRAY_BUFFER, vertices->size() * sizeof(GLfloat), vertices->data(), GL_STATIC_DRAW);
        glVertexAttribPointer(sh_position_attr, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(sh_position_attr);
        VBO_ids[VERTEX_VBO] = vertices_VBO.bufferId();

        normals_VBO.create();
        normals_VBO.bind();
        glBufferData(GL_ARRAY_BUFFER, normals->size() * sizeof(GLfloat), normals->data(), GL_STATIC_DRAW);
        glVertexAttribPointer(sh_normals_attr, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(sh_normals_attr);
        VBO_ids[NORMAL_VBO] = normals_VBO.bufferId();

        VAO_buffers_list->append(VBO_ids);
        VAO_list->append(VAO);
        VAO->release();
    }

    qDebug("[RENDER ENGINE] %d VAOs initialized [%d ms]", vertex_buffer_size, timer.elapsed());
}

void RenderEngine::init_background_gradient()
{
    GLfloat bg_vertices[] = {
        -1.0f, 1.0f, 1.0f,
         1.0f, 1.0f, 1.0f,
         1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f, 1.0f
    };

    background_VAO->bind();
    background_VBO->bind();

    glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(GLfloat), bg_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(sh_bg_position_attr, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(sh_bg_position_attr);

    background_VBO->release();
    background_VAO->release();
}

/**
 * @brief RenderEngine::update Function called every time when voxels are updated.
 */
void RenderEngine::update()
{
    init_vao();
}

void RenderEngine::set_plane(GLfloat mouse_x, GLfloat mouse_y)
{
    if(mouse_x == -1 && plane_visible) mouse_x = plane_x;
    else if(mouse_x == -1 && !plane_visible) return;
    else plane_x = mouse_x;

    if(mouse_y == -1) mouse_y = plane_y;
    else if(mouse_y == -1 && !plane_visible) return;
    else plane_y = mouse_y;

    if(!plane_visible)
    {
        toggle_plane_button->setText("Hide plane");
        plane_visible = true;
    }

    parent->makeCurrent();

    // cast display coordinates to world coordinates, calculate plane position
    QVector4D projected_coords = QVector4D(mouse_x + ((float)parent->width()/2), - mouse_y + ((float)parent->height()/2), 0, 1);
    QVector4D camera_coords = camera->projection_matrix->inverted() * projected_coords;
    QVector3D picked_position = camera_coords.toVector3D();

    QMatrix4x4 plane_transformation = camera->rotation_matrix->inverted();
    plane_transformation.translate(0, picked_position.y(), 0);

    plane_VAO->bind();
    plane_VBO->bind();

    // transform default plane
    GLfloat vertex_coord = plane_size / (GLfloat)2;
    QVector3D v1 = plane_transformation * QVector3D(-vertex_coord, 0, -vertex_coord);
    QVector3D v2 = plane_transformation * QVector3D(vertex_coord, 0,  -vertex_coord);
    QVector3D v3 = plane_transformation * QVector3D(-vertex_coord, 0,  vertex_coord);
    QVector3D v4 = plane_transformation * QVector3D(vertex_coord, 0, vertex_coord);
    GLfloat vertices[] = { v1.x(), v1.y(), v1.z(), v2.x(), v2.y(), v2.z(), v3.x(), v3.y(), v3.z(), v4.x(), v4.y(), v4.z() };

    // draw plane
    glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(GLfloat), vertices, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(sh_planes_position_attr, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(sh_planes_position_attr);

    plane_VBO->release();
    plane_VAO->release();
    parent->doneCurrent();

    // calculate plane's equation
    QVector3D plane_normal = QVector3D::crossProduct(v2 - v1, v3 - v1).normalized();
    GLfloat D_coefficient = plane_normal.x() * v4.x() + plane_normal.y() * v4.y() + plane_normal.z() * v4.z();
    voxel_engine->set_plane_equation(QVector4D(plane_normal, -D_coefficient));
}

bool RenderEngine::toggle_plane()
{
    plane_visible = !plane_visible;
    return plane_visible;
}

/**
 * @brief RenderEngine::render function called every time we want to render frame. Actual rendering happens here.
 */
void RenderEngine::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    render_background();

    main_shader->bind();
    main_shader->setUniformValue(sh_transformation_uni, camera->get_matrix());
    main_shader->setUniformValue(sh_ambient_light_uni, light->ambient);

    main_shader->setUniformValue(sh_diffuse_light_uni, light->diffuse);

    main_shader->setUniformValue(sh_specular_light_uni, light->specular);
    main_shader->setUniformValue(sh_specular_material_uni, QVector3D(1.0f, 1.0f, 1.0f));
    main_shader->setUniformValue(sh_material_shininess_uni, 32);

    main_shader->setUniformValue(sh_camera_position_uni, camera->get_position());
    main_shader->setUniformValue(sh_post_processing_gamma_uni, gamma_level);
    main_shader->setUniformValue(sh_post_processing_contrast_uni, contrast_level);

    for(int i = 0; i < VAO_list->size(); i++)
    {
        QOpenGLVertexArrayObject *VAO = VAO_list->at(i);
        VAO->bind();

        QVector<GLushort>* indices = this->voxel_engine->indices_list->at(i);
        EBO->bind();
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices->size() * sizeof(GLushort), indices->data(), GL_STATIC_DRAW);

        glDrawElements(GL_TRIANGLES, indices->size(), GL_UNSIGNED_SHORT, 0);

        EBO->release();
        VAO->release();
    }
    main_shader->release();

    if(plane_visible) render_plane();
}

void RenderEngine::render_plane()
{
    plane_shader->bind();
    plane_shader->setUniformValue(sh_planes_transformation_uni, camera->get_matrix());

    plane_VAO->bind();
    EBO->bind();
    GLushort plane_indices[] = {0, 1, 2, 1, 3, 2};
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLushort), plane_indices, GL_DYNAMIC_DRAW);

    glDisable(GL_CULL_FACE);
    glDrawElements(GL_TRIANGLES, 6,  GL_UNSIGNED_SHORT, 0);
    glEnable(GL_CULL_FACE);

    EBO->release();
    plane_VAO->release();
    plane_shader->release();
}

void RenderEngine::render_background()
{
    background_shader->bind();
    background_shader->setUniformValue(sh_bg_color_one_uni, background_top_gradient_color);
    background_shader->setUniformValue(sh_bg_color_second_uni, background_bottom_gradient_color);
    background_shader->setUniformValue(sh_bg_resolution_uni, resolution);

    background_VAO->bind();
    EBO->bind();
    short plane_indices[] = {0, 1, 3, 1, 2, 3};
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLushort), plane_indices, GL_STATIC_DRAW);

    glDisable(GL_CULL_FACE);
    glDrawElements(GL_TRIANGLES, 6,  GL_UNSIGNED_SHORT, 0);
    glEnable(GL_CULL_FACE);

    EBO->release();
    background_VAO->release();
    background_shader->release();
}
