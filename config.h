#ifndef CONFIG
#define CONFIG

#define CONFIG_FILE_NAME "config.ini"

#include <QtOpenGL>
#include <QString>
#include <QSettings>
#include <QDebug>

class Config
{
public:
    static Config* instance();
    void saveConfig();

    QString import_dir;
    QString export_dir;
    QString screen_dir;
    QString screen_core;
    GLfloat gamma_level;
    GLfloat contrast_level;
    GLint export_filetype;

    GLint world_size;

    GLfloat fov;
    GLfloat z_near;
    GLfloat z_far;
    GLfloat start_camera_distance;
    GLint plane_size;

    GLfloat x_mouse_sens;
    GLfloat y_mouse_sens;
    GLfloat wheel_sens;

    qint64 buffer_size;

    QString stylesheet_name;
    QString bottom_gradient_color;
    QString top_gradient_color;

private:
    QSettings* settings;
    static Config* inst;
    Config();
    Config(Config const&){instance();}
};

#endif // CONFIG
