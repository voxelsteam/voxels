#ifndef CAMERA_H
#define CAMERA_H

#include <QtMath>
#include <QtOpenGL>
#include <QVector3D>
#include <QMatrix4x4>

#include "config.h"

class Camera
{
public:
    QMatrix4x4 *rotation_matrix;
    QMatrix4x4 *projection_matrix;

    Camera();
    ~Camera();

    QMatrix4x4 get_matrix();

    void zoom(float factor);
    void move(const QVector3D &vector);
    void rotate(const QVector3D &angles);
    void look_around(GLfloat yaw, GLfloat pitch);
    void reset();

    QVector3D get_position() { return QVector3D(camera_position.x(), camera_position.y(), -camera_position.z()); }
    void set_aspect_ratio(GLfloat aspect_ratio)
    {
        this->aspect_ratio = aspect_ratio;
        calculate_projection_matrix();
    }

private:
    GLfloat fov;
    GLfloat z_near;
    GLfloat z_far;
    GLfloat aspect_ratio;

    GLfloat yaw = 0.0f;
    GLfloat pitch = 0.0f;
    QVector3D unrotated_target_position;
    QVector3D target_position;
    GLfloat distance = 1.0f;

    QVector3D camera_default_position = QVector3D(0, 0, 1);
    QVector3D translation;
    QVector3D camera_position;

    QVector3D world_default_up = QVector3D(0, 1, 0);
    QVector3D world_up = QVector3D(0, 1, 0);
    QVector3D camera_up;
    QVector3D camera_right;

    QMatrix4x4 *look_at_left_multiplier;
    QMatrix4x4 *look_at_right_multiplier;
    QMatrix4x4 *look_at;

    void calculate_projection_matrix();
};

#endif // CAMERA_H
