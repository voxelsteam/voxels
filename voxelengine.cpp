#include "voxelengine.h"

VoxelEngine::VoxelEngine(): MAX_BUFFER_SIZE(Config::instance()->buffer_size)
{
    vertices = new QVector<GLfloat>;
    normals = new QVector<GLfloat>;
    indices = new QVector<GLushort>;

    vertices_list = new QList<QVector<GLfloat>*>();
    normals_list = new QList<QVector<GLfloat>*>();
    indices_list = new QList<QVector<GLushort>*>();

    is_world_initialized = false;
    center_offset = Config::instance()->world_size / 2;
}

VoxelEngine::~VoxelEngine()
{
    qDeleteAll(*vertices_list);
    delete vertices_list;
    vertices_list = nullptr;

    qDeleteAll(*normals_list);
    delete normals_list;
    normals_list = nullptr;

    qDeleteAll(*indices_list);
    delete indices_list;
    indices_list = nullptr;

    destroy_world();
}

/**
 * @brief VoxelEngine::initialize_world allocates world data structure, clears all vertices, indices and texture coordinates
 */
void VoxelEngine::initialize_world(GLint size)
{
    if(size != -1) WORLD_SIZE = size;

    timer.start();

    delete world;
    world = new SparseMatrix3D(CHUNKS, WORLD_SIZE / CHUNKS);

    qDebug("[VOXEL ENGINE] Initialized world of size %d [%d ms]", WORLD_SIZE, timer.elapsed());
}

/**
 * @brief VoxelEngine::construct_world Constucts new world using world array as reference.
 * Must not be called before engine initialization!
 */
void VoxelEngine::construct_world()
{
    timer.restart();

    current_index = 0;
    vertex_coords_in_buffer_processed = 0;

    GLuint chunks_no = world->chunks_number;
    GLint concurrent_thread_supported = std::thread::hardware_concurrency();
    if(concurrent_thread_supported < 1) concurrent_thread_supported = 1; //not able to detect

    int chunks_per_thread = chunks_no / concurrent_thread_supported;

    int interval_start = 0, interval_end = 0;
    QVector3D from, to;

    for(int i = 0; i < concurrent_thread_supported; i++)
    {
        interval_end = chunks_per_thread *( i + 1);
        from = QVector3D(0, 0, interval_start);
        to = QVector3D(chunks_no, chunks_no,  interval_end);

        if(marching_cubes_enabled)
            thread_pool.push_back(std::thread(&VoxelEngine::construct_world_marching_cubes_caller, this, from, to));
        else
            thread_pool.push_back(std::thread(&VoxelEngine::construct_world_voxels_caller, this, from, to));

         interval_start = interval_end;
    }

    for (auto& thread : thread_pool) thread.join();
    thread_pool.clear();
    save_buffers();

    qDebug() << "CONSTRUCT WORLD" << timer.elapsed();
}

/**
 * Used to call construct_world_old_method_job by std::thread constructor
 * @brief VoxelEngine::construct_world_old_method_job_caller
 * @param this_engine
 * @param from
 * @param to
 */
void VoxelEngine::construct_world_voxels_caller(VoxelEngine* this_engine, QVector3D from, QVector3D to)
{
    this_engine->construct_world_voxels(from, to);
}

/**
 * Old boring method for generating geometry
 * @brief VoxelEngine::construct_world_old_method_job
 * @param from
 * @param to
 */
void VoxelEngine::construct_world_voxels(QVector3D from, QVector3D to)
{
    bool****** world_mat = world->chunks;
    GLushort chunk_size = world->chunk_size;

    for(int i = from.x(); i < to.x(); i++)
        for(int  j = from.y(); j < to.y(); j++)
            for(int k = from.z(); k < to.z(); k++)
            {
                bool*** chunk = world_mat[i][j][k];
                if(chunk == nullptr) continue;

                for(int chunk_i = 0; chunk_i < chunk_size; chunk_i++)
                    for(int chunk_j = 0; chunk_j < chunk_size; chunk_j++)
                        for(int chunk_k = 0; chunk_k < chunk_size; chunk_k++)
                        {
                            if(chunk[chunk_i][chunk_j][chunk_k])
                            {
                                //uzialize cube...
                                QVector3D position(chunk_i + i*chunk_size - center_offset, chunk_j + j*chunk_size - center_offset, chunk_k + k*chunk_size - center_offset);

                                // Calculate the position of the vertices on the top face.
                                QVector3D topLeftFront = position + TOP_LEFT_FRONT_OFFSET;
                                QVector3D topLeftBack = position +  TOP_LEFT_BACK_OFFSET;
                                QVector3D topRightFront = position + TOP_RIGHT_FRONT_OFFSET;
                                QVector3D topRightBack = position + TOP_RIGHT_BACK_OFFSET;

                                // Calculate the position of the vertices on the bottom face.
                                QVector3D btmLeftFront = position + BOTTOM_LEFT_FRONT;
                                QVector3D btmLeftBack = position + BOTTOM_LEFT_BACK;
                                QVector3D btmRightFront = position + BOTTOM_RIGHT_FRONT;
                                QVector3D btmRightBack = position + BOTTOM_RIGHT_BACK;

                                mutex.lock();
                                if(MAX_BUFFER_SIZE - vertex_coords_in_buffer_processed <= -1)
                                {
                                    save_buffers();
                                    current_index = 0;
                                    vertex_coords_in_buffer_processed = 0;
                                }
                                mutex.unlock();

                                //FRONT FACE z-1
                                if(!world->get(i, j, chunk_k - 1 < 0 ? k - 1 : k, chunk_i, chunk_j, chunk_k - 1 < 0 ? chunk_size - 1 : chunk_k - 1))
                                {
                                    mutex.lock();
                                    add_vertex(topLeftFront, NORMAL_FRONT);
                                    add_vertex(btmLeftFront, NORMAL_FRONT);
                                    add_vertex(topRightFront, NORMAL_FRONT);

                                    add_vertex(btmLeftFront, NORMAL_FRONT);
                                    add_vertex(btmRightFront, NORMAL_FRONT);
                                    add_vertex(topRightFront, NORMAL_FRONT);

                                    for(int i=0; i<INDICES_PER_WALL; i++){
                                        indices->append(current_index + i);
                                    }
                                    current_index += INDICES_PER_WALL;
                                    vertex_coords_in_buffer_processed += COORDS_PER_WALL;
                                    mutex.unlock();
                                }

                                //BACK FACE z+1
                                if(!world->get(i, j, chunk_k + 1 == chunk_size ? k + 1 : k, chunk_i, chunk_j, chunk_k + 1 == chunk_size ? 0 : chunk_k + 1))
                                {
                                    mutex.lock();
                                    add_vertex(topLeftBack, NORMAL_BACK);
                                    add_vertex(topRightBack, NORMAL_BACK);
                                    add_vertex(btmLeftBack, NORMAL_BACK);

                                    add_vertex(btmLeftBack, NORMAL_BACK);
                                    add_vertex(topRightBack, NORMAL_BACK);
                                    add_vertex(btmRightBack, NORMAL_BACK);

                                    for(int i=0; i<INDICES_PER_WALL; i++){
                                        indices->append(current_index + i);
                                    }
                                    current_index += INDICES_PER_WALL;
                                    vertex_coords_in_buffer_processed += COORDS_PER_WALL;
                                    mutex.unlock();
                                }

                                //TOP FACE y+1
                                if(!world->get(i, chunk_j + 1 == chunk_size ? j + 1 : j, k, chunk_i, chunk_j + 1 == chunk_size ? 0 : chunk_j + 1, chunk_k))
                                {
                                    mutex.lock();
                                    add_vertex(topLeftFront, NORMAL_TOP);
                                    add_vertex(topRightBack, NORMAL_TOP);
                                    add_vertex(topLeftBack, NORMAL_TOP);

                                    add_vertex(topLeftFront, NORMAL_TOP);
                                    add_vertex(topRightFront, NORMAL_TOP);
                                    add_vertex(topRightBack, NORMAL_TOP);

                                    for(int i=0; i<INDICES_PER_WALL; i++){
                                        indices->append(current_index + i);
                                    }
                                    current_index += INDICES_PER_WALL;
                                    vertex_coords_in_buffer_processed += COORDS_PER_WALL;
                                    mutex.unlock();
                                }

                                //BOTTOM FACE y-1
                                if(!world->get(i, chunk_j - 1 < 0 ? j - 1 : j, k, chunk_i, chunk_j - 1 < 0 ? chunk_size - 1 : chunk_j - 1, chunk_k))
                                {
                                    mutex.lock();
                                    add_vertex(btmLeftFront, NORMAL_BOTTOM);
                                    add_vertex(btmLeftBack, NORMAL_BOTTOM);
                                    add_vertex(btmRightBack, NORMAL_BOTTOM);

                                    add_vertex(btmLeftFront, NORMAL_BOTTOM);
                                    add_vertex(btmRightBack, NORMAL_BOTTOM);
                                    add_vertex(btmRightFront, NORMAL_BOTTOM);

                                    for(int i=0; i<INDICES_PER_WALL; i++){
                                        indices->append(current_index + i);
                                    }
                                    current_index += INDICES_PER_WALL;
                                    vertex_coords_in_buffer_processed += COORDS_PER_WALL;
                                    mutex.unlock();
                                }

                                //LEFT FACE x-1
                                if(!world->get(chunk_i - 1 < 0 ? i - 1 : i, j, k, chunk_i - 1 < 0 ? chunk_size - 1 : chunk_i - 1, chunk_j, chunk_k))
                                {
                                    mutex.lock();
                                    add_vertex(topLeftFront, NORMAL_LEFT);
                                    add_vertex(btmLeftBack, NORMAL_LEFT);
                                    add_vertex(btmLeftFront, NORMAL_LEFT);

                                    add_vertex(topLeftBack, NORMAL_LEFT);
                                    add_vertex(btmLeftBack, NORMAL_LEFT);
                                    add_vertex(topLeftFront, NORMAL_LEFT);

                                    for(int i=0; i<INDICES_PER_WALL; i++){
                                        indices->append(current_index + i);
                                    }
                                    current_index += INDICES_PER_WALL;
                                    vertex_coords_in_buffer_processed += COORDS_PER_WALL;
                                    mutex.unlock();
                                }

                                //RIGHT FACE x+1
                                if(!world->get(chunk_i + 1 == chunk_size ? i + 1 : i, j, k, chunk_i + 1 == chunk_size ? 0 : chunk_i + 1, chunk_j, chunk_k))
                                {
                                    mutex.lock();
                                    add_vertex(topRightFront, NORMAL_RIGHT);
                                    add_vertex(btmRightFront, NORMAL_RIGHT);
                                    add_vertex(btmRightBack, NORMAL_RIGHT);

                                    add_vertex(topRightBack, NORMAL_RIGHT);
                                    add_vertex(topRightFront, NORMAL_RIGHT);
                                    add_vertex(btmRightBack, NORMAL_RIGHT);

                                    for(int i=0; i<INDICES_PER_WALL; i++){
                                        indices->append(current_index + i);
                                    }
                                    current_index += INDICES_PER_WALL;
                                    vertex_coords_in_buffer_processed += COORDS_PER_WALL;
                                    mutex.unlock();
                                }
                            }
                        }
            }
}

/**
 * @brief VoxelEngine::construct_world Constucts new world using world array as reference.
 * Must not be called before engine initialization!
 */
void VoxelEngine::construct_world_marching_cubes_caller(VoxelEngine* this_engine, QVector3D from, QVector3D to)
{
    this_engine->construct_world_marching_cubes(from, to);
}

/**
 * Marching cubes method for generating geometry
 * @brief VoxelEngine::construct_world_marching_cubes_job
 * @param from
 * @param to
 */
void VoxelEngine::construct_world_marching_cubes(QVector3D from, QVector3D to)
{
    bool****** world_mat = world->chunks;

     for(int i = from.x(); i < to.x(); i++)
         for(int  j = from.y(); j < to.y(); j++)
             for(int k = from.z(); k < to.z(); k++)
             {
                 bool*** chunk = world_mat[i][j][k];
                 if(chunk != nullptr)
                 {
                     if(i > 0 && world_mat[i - 1][j][k] == nullptr) iterate_chunk(i - 1, j, k);
                     if(j > 0 && world_mat[i][j - 1][k] == nullptr) iterate_chunk(i, j - 1, k);
                     if(k > 0 && world_mat[i][j][k - 1] == nullptr) iterate_chunk(i, j, k - 1);

                     iterate_chunk(i, j, k);
                 }
             }
}

void VoxelEngine::iterate_chunk(int i, int j, int k)
{
    int chunk_size = world->chunk_size;

    for(int chunk_i = 0; chunk_i < chunk_size; chunk_i++)
        for(int chunk_j = 0; chunk_j < chunk_size; chunk_j++)
            for(int chunk_k = 0; chunk_k < chunk_size; chunk_k++)
            {
                bool i_out = chunk_i + 1 >= chunk_size;
                bool j_out = chunk_j + 1 >= chunk_size;
                bool k_out = chunk_k + 1 >= chunk_size;

                float gridval[8];
                gridval[0] = world->get(i, j, k_out ? k + 1 : k, chunk_i, chunk_j, k_out ? 0 : chunk_k + 1);
                gridval[1] = world->get(i_out ? i + 1 : i, j, k_out ? k+1 : k, i_out ? 0 : chunk_i + 1, chunk_j, k_out ? 0 : chunk_k + 1);
                gridval[2] = world->get(i_out ? i + 1 : i, j_out ? j + 1 : j, k_out ? k + 1 : k, i_out ? 0 : chunk_i + 1, j_out ? 0 : chunk_j + 1, k_out ? 0 : chunk_k + 1);
                gridval[3] = world->get(i, j_out ? j + 1 : j, k_out ? k + 1 : k, chunk_i, j_out ? 0 : chunk_j + 1, k_out ? 0 : chunk_k + 1);
                gridval[4] = world->get(i, j, k, chunk_i, chunk_j, chunk_k);
                gridval[5] = world->get(i_out ? i + 1 : i, j, k, i_out ? 0 : chunk_i + 1, chunk_j, chunk_k);
                gridval[6] = world->get(i_out ? i + 1 : i, j_out ? j+1 : j, k, i_out ? 0 : chunk_i + 1, j_out ? 0 : chunk_j + 1, chunk_k);
                gridval[7] = world->get(i, j_out ? j + 1 : j, k, chunk_i, j_out ? 0 : chunk_j + 1, chunk_k);

                if(gridval[0] || gridval[1] || gridval[2] || gridval[3] || gridval[4] || gridval[5] || gridval[6] || gridval[7])
                {
                    mutex.lock();
                    if(MAX_BUFFER_SIZE - vertex_coords_in_buffer_processed <= -1)
                    {
                        save_buffers();
                        current_index = 0;
                        vertex_coords_in_buffer_processed = 0;
                    }
                    mutex.unlock();

                    float i_casted = chunk_i + i*chunk_size - center_offset;
                    float j_casted = chunk_j + j*chunk_size - center_offset;
                    float k_casted =chunk_k + k*chunk_size - center_offset;

                    QVector3D gridpos[] =
                    {
                        QVector3D(i_casted, j_casted, k_casted + 1),
                        QVector3D(i_casted + 1, j_casted, k_casted + 1),
                        QVector3D(i_casted + 1, j_casted + 1, k_casted + 1),
                        QVector3D(i_casted, j_casted + 1, k_casted + 1),

                        QVector3D(i_casted, j_casted, k_casted),
                        QVector3D(i_casted + 1, j_casted, k_casted),
                        QVector3D(i_casted + 1, j_casted + 1, k_casted),
                        QVector3D(i_casted, j_casted + 1, k_casted)
                    };

                    polygonise(gridpos, gridval, 1.0);
                }
            }
}

void VoxelEngine::polygonise(QVector3D gridpos[8], float gridval[8], float isolevel)
{
    QVector3D vertlist[12];
    int cubeindex = 0;
    if (gridval[0] < isolevel) cubeindex |= 1;
    if (gridval[1] < isolevel) cubeindex |= 2;
    if (gridval[2] < isolevel) cubeindex |= 4;
    if (gridval[3] < isolevel) cubeindex |= 8;
    if (gridval[4] < isolevel) cubeindex |= 16;
    if (gridval[5] < isolevel) cubeindex |= 32;
    if (gridval[6] < isolevel) cubeindex |= 64;
    if (gridval[7] < isolevel) cubeindex |= 128;

    if (edgeTable[cubeindex] == 0) return;
    if (edgeTable[cubeindex] & 1)    vertlist[0] =  interpolate_vertex(isolevel,gridpos[0],gridpos[1],gridval[0],gridval[1]);
    if (edgeTable[cubeindex] & 2)    vertlist[1] =  interpolate_vertex(isolevel,gridpos[1],gridpos[2],gridval[1],gridval[2]);
    if (edgeTable[cubeindex] & 4)    vertlist[2] =  interpolate_vertex(isolevel,gridpos[2],gridpos[3],gridval[2],gridval[3]);
    if (edgeTable[cubeindex] & 8)    vertlist[3] =  interpolate_vertex(isolevel,gridpos[3],gridpos[0],gridval[3],gridval[0]);
    if (edgeTable[cubeindex] & 16)   vertlist[4] =  interpolate_vertex(isolevel,gridpos[4],gridpos[5],gridval[4],gridval[5]);
    if (edgeTable[cubeindex] & 32)   vertlist[5] =  interpolate_vertex(isolevel,gridpos[5],gridpos[6],gridval[5],gridval[6]);
    if (edgeTable[cubeindex] & 64)   vertlist[6] =  interpolate_vertex(isolevel,gridpos[6],gridpos[7],gridval[6],gridval[7]);
    if (edgeTable[cubeindex] & 128)  vertlist[7] =  interpolate_vertex(isolevel,gridpos[7],gridpos[4],gridval[7],gridval[4]);
    if (edgeTable[cubeindex] & 256)  vertlist[8] =  interpolate_vertex(isolevel,gridpos[0],gridpos[4],gridval[0],gridval[4]);
    if (edgeTable[cubeindex] & 512)  vertlist[9] =  interpolate_vertex(isolevel,gridpos[1],gridpos[5],gridval[1],gridval[5]);
    if (edgeTable[cubeindex] & 1024) vertlist[10] = interpolate_vertex(isolevel,gridpos[2],gridpos[6],gridval[2],gridval[6]);
    if (edgeTable[cubeindex] & 2048) vertlist[11] = interpolate_vertex(isolevel,gridpos[3],gridpos[7],gridval[3],gridval[7]);

    for(int c = 0; triTable[cubeindex][c] != -1; c+=3)
    {
        //get face vertices
        QVector3D vert1 = vertlist[triTable[cubeindex][c]];
        QVector3D vert2 = vertlist[triTable[cubeindex][c + 1]];
        QVector3D vert3 = vertlist[triTable[cubeindex][c + 2]];

        //calculate normals
        QVector3D a = vert1 - vert2;
        QVector3D b = vert1 - vert3;
        QVector3D normal = (QVector3D::crossProduct(a,b));
        normal.normalize();
        normal*=-1;
        normal.setZ(normal.z() * -1);

        mutex.lock();

        add_vertex(vert1, normal);
        add_vertex(vert2, normal);
        add_vertex(vert3, normal);

        for(int f = 0; f < 3; f++) indices->append(current_index + f);

        current_index += 3;
        vertex_coords_in_buffer_processed += 9; //3*3

        mutex.unlock();
    }
}

QVector3D VoxelEngine::interpolate_vertex(float isolevel, QVector3D p1, QVector3D p2, float val1, float val2)
{
    float mu;
    if(qAbs(isolevel - val1) < 0.00001) return p1;
    if(qAbs(isolevel - val2) < 0.00001) return p2;
    if(qAbs(val1 - val2) < 0.00001) return p1;

    mu = (isolevel - val1) / (val2 - val1);
    float px = p1.x() + mu * (p2.x() - p1.x());
    float py = p1.y() + mu * (p2.y() - p1.y());
    float pz = p1.z() + mu * (p2.z() - p1.z());

    return QVector3D(px,py,pz);
}

void VoxelEngine::intersect(bool flip)
{
    timer.restart();

    GLdouble A = plane_equation.x();
    GLdouble B = plane_equation.y();
    GLdouble C = plane_equation.z();
    GLdouble D = plane_equation.w();

    bool****** world_mat = world->chunks;
    GLushort chunks_number = world->chunks_number;
    GLushort chunk_size = world->chunk_size;

    for(int i = 0; i < chunks_number; i++)
        for(int  j = 0; j < chunks_number; j++)
            for(int k = 0; k < chunks_number; k++)
            {
                bool*** chunk = world_mat[i][j][k];
                if(chunk == nullptr) continue;

                for(int chunk_i =0; chunk_i < chunk_size; chunk_i++)
                    for(int chunk_j =0; chunk_j < chunk_size; chunk_j++)
                        for(int chunk_k = 0; chunk_k < chunk_size; chunk_k++)
                        {
                            if(!chunk[chunk_i][chunk_j][chunk_k]) continue;
                            GLdouble result=
                                    A * (chunk_i + i*chunk_size - center_offset) +
                                    B * (chunk_j + j*chunk_size - center_offset) +
                                    C * (chunk_k + k * chunk_size - center_offset) +
                                    D;

                            if(flip ? result > 0 : result < 0) chunk[chunk_i][chunk_j][chunk_k] = false;
                        }
            }

    qDebug("[VOXEL ENGINE] Intersection generated. [%d ms]", timer.elapsed());
}

void VoxelEngine::add_vertex(QVector3D position, QVector3D normal)
{
    vertices->append(position.x());
    vertices->append(position.y());
    vertices->append(position.z());

    normals->append(normal.x());
    normals->append(normal.y());
    normals->append(normal.z());
}

void VoxelEngine::save_buffers()
{
    vertices_list->append(vertices);
    vertices = new QVector<GLfloat>();

    normals_list->append(normals);
    normals = new QVector<GLfloat>();

    indices_list->append(indices);
    indices = new QVector<GLushort>();
}

void VoxelEngine::destroy_world()
{
    delete world;
}

void VoxelEngine::clear_buffers()
{
    timer.start();

    // clear buffers
    qDeleteAll(*vertices_list);
    vertices_list->clear();

    qDeleteAll(*normals_list);
    normals_list->clear();

    qDeleteAll(*indices_list);
    indices_list->clear();

    qDebug("[VOXEL ENGINE] Buffers cleared [%d ms]", timer.elapsed());
}

void VoxelEngine::clear_world()
{
    initialize_world(); // reinitialize world
}

bool VoxelEngine::is_in_the_middle(int x, int y, int z)
{
    if ((x + 1 < WORLD_SIZE && world->get(x + 1, y, z)) || x + 1 == WORLD_SIZE)
        if ((y + 1 < WORLD_SIZE && world->get(x, y + 1, z)) || y + 1 == WORLD_SIZE)
            if ((z + 1 < WORLD_SIZE && world->get(x, y, z + 1)) || z + 1 == WORLD_SIZE)
                if ((x - 1 >= 0 && world->get(x - 1, y, z)) || x - 1 < 0)
                    if ((y - 1 >= 0 && world->get(x, y - 1, z)) || y - 1 < 0)
                        if ((z - 1 >= 0 && world->get(x, y, z - 1)) || z - 1 < 0)
                            return true;
    return false;
}
